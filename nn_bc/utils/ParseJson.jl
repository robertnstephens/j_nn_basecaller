
############################################
# author: Robert Stephens                  #
# email:  robert.stephens@nanoporetech.com #
############################################

module ParseJson

import JSON


export parse_fields
function parse_fields(json_file)

    j = JSON.parsefile(json_file; dicttype=Dict, use_mmap=true)

    w = Dict{String, Array{Float64}}()

    #blstm_0                           
    #internal : (33152,) lW_0_0, lW_1_0 (2*16384) 2*[4 x size3 x size3 (64*64)] + p_0_0, p_0_1 (192*2) 2*[3 x size3 (128)]
    #bias : (512,) b_0_0 256, b_1_0 256 = 512 2*[4 x size3 (128)]
    #input : (6144,) iW_0_0 3072, iW_1_0 3072 = 6144 2*[4 x 12 x size3 (64)]

    size1 = 2*j["layers"][2]["size"] * j["layers"][1]["size"]
    
    
    size2 = j["layers"][2]["size"] 
    size3 = Int(size2/2)

    internal = j["weights"]["blstm_0"]["internal"]

    #size_layer_2 = 128, size_layer_1 = 12 

    #iW1 is 12*64*0 -> 12*64*1, 12*64*2 -> 12*64*3

    #iW2 is 12*64*1 -> 12*64*2, 12*64*3 -> 12*64*4

    #nn is 3D from npy iW1 (4, -1, size) so (4, 12, 64) -> reshaped to (12, 4*64)
    #json is 1D array

    #hcat(b, a[:,1:2])


    w["br1_iW1"] = j["weights"]["blstm_0"]["input"][1:12*64]
    w["br1_iW2"] = j["weights"]["blstm_0"]["input"][1+12*64:12*64*2]
   
    w["br1_b1"] = j["weights"]["blstm_0"]["bias"][1:64]
    w["br1_b2"] = j["weights"]["blstm_0"]["bias"][1+64:64*2]


    unit_size3 = size3*size3

    w["br1_lW1"] = internal[1:unit_size3]
    w["br1_lW2"] = internal[1+unit_size3:2*unit_size3]

    for i in 1:3
        append!(w["br1_iW1"], j["weights"]["blstm_0"]["input"][1+12*64*(2*i):12*64*(2*i+1)])
        append!(w["br1_iW2"], j["weights"]["blstm_0"]["input"][1+12*64*(2*i+1):12*64*(2*i+2)])

        append!(w["br1_b1"], j["weights"]["blstm_0"]["bias"][1+64*(2*i):64*(2*i+1)])
        append!(w["br1_b2"], j["weights"]["blstm_0"]["bias"][1+64*(2*i+1):64*(2*i+2)])

        append!(w["br1_lW1"], internal[1+unit_size3*(2*i):unit_size3*(2*i+1)])
        append!(w["br1_lW2"], internal[1+unit_size3*(2*i+1):unit_size3*(2*i+2)])

    end


    unit_size = 4*size3*size3 
    temp = internal[1+2*unit_size:end]

    w["br1_p1"] = temp[1:64]
    w["br1_p2"] = temp[1+64:64*2]

    for i in 1:2
        append!(w["br1_p1"], temp[1+64*(2*i):64*(2*i+1)])
        append!(w["br1_p2"], temp[1+64*(2*i+1):64*(2*i+2)])
    end


    size1 = 2*j["layers"][4]["size"] * j["layers"][3]["size"]
   
    size2 = j["layers"][2]["size"] 
    size3 = Int(size2/2)

    internal = j["weights"]["blstm_2"]["internal"]
  
    unit_size = 4*size3*size3 
    
    w["br2_iW1"] = j["weights"]["blstm_2"]["input"][1:64*64]
    w["br2_iW2"] = j["weights"]["blstm_2"]["input"][1+64*64:64*64*2]

    w["br2_b1"] = j["weights"]["blstm_2"]["bias"][1:64]
    w["br2_b2"] = j["weights"]["blstm_2"]["bias"][1+64:64*2]

    unit_size3 = size3*size3

    w["br2_lW1"] = internal[1:unit_size3]
    w["br2_lW2"] = internal[1+unit_size3:2*unit_size3]

    for i in 1:3
        append!(w["br2_iW1"], j["weights"]["blstm_2"]["input"][1+64*64*(2*i):64*64*(2*i+1)])
        append!(w["br2_iW2"], j["weights"]["blstm_2"]["input"][1+64*64*(2*i+1):64*64*(2*i+2)])
    
        append!(w["br2_b1"], j["weights"]["blstm_2"]["bias"][1+64*(2*i):64*(2*i+1)])
        append!(w["br2_b2"], j["weights"]["blstm_2"]["bias"][1+64*(2*i+1):64*(2*i+2)])

        append!(w["br2_lW1"], internal[1+unit_size3*(2*i):unit_size3*(2*i+1)])
        append!(w["br2_lW2"], internal[1+unit_size3*(2*i+1):unit_size3*(2*i+2)])

    end

    unit_size = 4*size3*size3 
    temp = internal[1+2*unit_size:end]

    w["br2_p1"] = temp[1:64]
    w["br2_p2"] = temp[1+64:64*2]

    for i in 1:2
        append!(w["br2_p1"], temp[1+64*(2*i):64*(2*i+1)])
        append!(w["br2_p2"], temp[1+64*(2*i+1):64*(2*i+2)])
    end

    w["ff1_W"] = j["weights"]["feedforward_tanh_1"]["input"]
    w["ff1_b"] = j["weights"]["feedforward_tanh_1"]["bias"]
   
    w["ff2_W"] = j["weights"]["feedforward_tanh_3"]["input"]
    w["ff2_b"] = j["weights"]["feedforward_tanh_3"]["bias"]

    w["sm_W"] = j["weights"]["softmax_4"]["input"]
    w["sm_b"] = j["weights"]["softmax_4"]["bias"]

    return w
end

end #module ParseJson


