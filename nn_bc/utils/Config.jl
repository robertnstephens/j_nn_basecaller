
############################################
# author: Robert Stephens                  #
# email:  robert.stephens@nanoporetech.com #
############################################

module Config

export _ETA, _BASES, _BASES_LEN, _KMER_LEN, _NUM_KMERS, _NSTEP, _NSKIP, _STEP_FACTOR, _SKIP_FACTOR

_ETA = 1e-30
_BASES = ["A", "C", "G", "T"]
_BASES_LEN = length(_BASES)
_KMER_LEN = 5
_NUM_KMERS = _BASES_LEN^_KMER_LEN
_NSTEP = _BASES_LEN
_NSKIP = _NSTEP^2
_STEP_FACTOR = log(e, _NSTEP)
_SKIP_FACTOR = log(e, _NSKIP)

#TODO pull from JSON config file
#R9 
#ed_params: {'thresholds': [2.0, 1.1], 'window_lengths': [5, 10], 'peak_height': 1.2}
#min_len: 500
#section: template
#max_len: 50000
#sloika_model: False
#event_detect: True


#default (R9_4) 
#ed_params: {'thresholds': [1.4, 1.1], 'window_lengths': [3, 6], 'peak_height': 0.2}
#min_len: 500
#section: template
#max_len: 50000
#sloika_model: False
#event_detect: True


export gen_kmers
function gen_kmers()
    kmers = []
    
    for i in 0:(_BASES_LEN^_KMER_LEN)-1
    
        #0x3FF
        #11 11 11 11 11
        #a, c, g, t 
    
        mask = 0x3
        val = i
    
        next_kmer = ["X" for _ in 1:_KMER_LEN]
        for j in 1:_KMER_LEN
            index = val & mask
            next_kmer[end-j+1] = _BASES[index+1] 
            val >>= 2
        end
        push!(kmers, join(next_kmer))
    end
   
    bad_kmer = ["X" for _ in 1:_KMER_LEN]
    push!(kmers, join(bad_kmer))

    return kmers
end


end

