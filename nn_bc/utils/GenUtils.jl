
############################################
# author: Robert Stephens                  #
# email:  robert.stephens@nanoporetech.com #
############################################

module GenUtils

export kmer_overlap
function kmer_overlap(kmers; moves=nothing, it=false)
    #From a list of kmers return the character shifts between them.
    #(Movement from i to i+1 entry, e.g. [AATC,ATCG] returns [0,1]).

    #:param kmers: sequence of kmer strings.
    #:param moves: allowed movements, if None all movements to length of kmer are allowed.
    #:param it: yield values instead of returning a list.

    #Allowed moves may be specified in moves argument in order of preference.

    if it
        #return kmer_overlap_gen(kmers, moves)
    else
        return kmer_overlap_gen(kmers, moves=moves)
    end
end



#number of bases moved - e.g. 1 for TAAC, AACA; 2 for TAAC, ACGA; 0 for AACT, AACT
function kmer_overlap_gen(kmers; moves=nothing)
    #From a list of kmers return the character shifts between them.
    #(Movement from i to i+1 entry, e.g. [AATC,ATCG] returns [0,1]).
    #Allowed moves may be specified in moves argument in order of preference.

    #:param moves: allowed movements, if None all movements to length of kmer are allowed.

    first, ret = true, [0]
    
    for i in 2:size(kmers, 1)
        last_kmer, this_kmer = kmers[i-1], kmers[i]

        if first
            if moves == nothing
                l = length(this_kmer)
                moves = collect( 0:(l + 1) )
            end 
            first = false
        end

        l = length(this_kmer)

        for j in moves
            if j < 0
                if last_kmer[1:-j,:] == this_kmer[-j+1:end]
                    push!(ret, j) 
                    break
                end
            elseif j > 0 && j < l
                if last_kmer[j+1:l] == this_kmer[1:end-j]
                    push!(ret, j) 
                    break
                end
            elseif j == 0
                if last_kmer == this_kmer
                    push!(ret, 0) 
                    break
                end
            else
                push!(ret, l) 
                break
            end
        end #end for loop through moves
    end #end for loop through kmers
    return ret
end

export clip
function clip(x::Real, lo::Real, hi::Real)::promote_type(typeof(x), typeof(lo), typeof(hi))
    if x < lo
        return lo
    elseif x > hi
        return hi
    else
        return x
    end
end


end #module GenUtils
