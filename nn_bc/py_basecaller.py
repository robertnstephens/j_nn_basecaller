#! /usr/bin/env python3

import sys, time, os, argparse, random

import numpy as np
from sklearn.preprocessing import MinMaxScaler

# pip3 install graphviz, pydot-ng

import mxnet as mx

from keras.preprocessing import sequence
from keras.models import Sequential, model_from_json
from keras.layers import Dense, Dropout, Embedding, LSTM, Bidirectional, TimeDistributed, GRU
from keras.utils import np_utils
from keras.callbacks import Callback, ModelCheckpoint, EarlyStopping

from keras import optimizers
#optimizer = "rmsprop"
#optimizer = optimizers.SGD(lr=1e-5, momentum=0.9, decay=0.0, nesterov=True)
optimizer = optimizers.Adam(lr=0.01, beta_1=0.9, beta_2=0.999, epsilon=1e-08, decay=0.0)
#optimizer = optimizers.SGD(lr=1e-1, momentum=0.9, decay=1e-6, nesterov=True)


# limit cores with keras 
import tensorflow as tf
from keras.backend.tensorflow_backend import set_session

use_num_cores = 1
config = tf.ConfigProto(intra_op_parallelism_threads=use_num_cores, inter_op_parallelism_threads=use_num_cores, \
                        allow_soft_placement=True, device_count = {'CPU': use_num_cores})
session = tf.Session(config=config)
set_session(session)

import theano
theano.config.openmp = False
#OMP_NUM_THREADS=4 python ./run.py

import torch
import torch.nn as nn
from torch.autograd import Variable
#torch.set_default_tensor_type('torch.cuda.FloatTensor') #TODO

import matplotlib.pyplot as plt

import logging
logging.basicConfig(filename="training.log",level=logging.INFO)


sys.path.append("./py/")
import Wraph5py, PostUtils, SeqUtils, KLSTM

__ETA__ = np.finfo(np.float32).tiny


features = 12
batch_size = 20
train_ratio = 0.8
time_steps = 1
kmer_len = 4
learning_rate = 1e-3

run_prefix = "gru_rev"

'''
mxnet:
batch_size confused with seq_len

batch_size = 10

and:

with symbol transpose

RuntimeError: simple_bind error. Arguments:
target: (10, 65)
data: (10, 1, 12)

without symbol transpose

RuntimeError: simple_bind error. Arguments:
target: (10, 65)
data: (10, 1, 12)

'''

num_epochs = 2

alphabet="ACGT"
num_classes = pow(4, kmer_len) + 1
blstm_0 = blstm_2 = 60

feedforward_tanh_1 = 64 
feedforward_tanh_3 = num_classes 
softmax_4 = num_classes 

dropout = 0.0
recurrent_dropout = 0.0

model = None

class TestCallback(Callback):
    def __init__(self, test_data):
        self.test_data = test_data

    def on_epoch_end(self, epoch, logs={}):
        x, y = self.test_data
        loss, acc = self.model.evaluate(x, y, verbose=0)
        out = '\nKeras test_loss: {}, test_acc: {}\n'.format(loss, acc)
        print(out)
        logging.info(out)


class ValCallback(Callback):
    def __init__(self, val_data):
        self.val_data = val_data

    def on_epoch_end(self, epoch, logs={}):
        x, y = self.val_data
        loss, acc = self.model.evaluate(x, y, verbose=0)
        out = '\nKeras val_loss: {}, test_acc: {}\n'.format(loss, acc)
        print(out)
        logging.info(out)

def build_sym(seq_len):
    data = mx.sym.var("data")  # Shape: (N, T, C) batch_size, seq_len, hidden
    target = mx.sym.var("target")  # Shape: (N, T, C)

    # new #
    #data = mx.sym.transpose(data, axes=(1, 0, 2))  # Shape: (T, N, C)
    #######

    stacked_rnn_cells = mx.rnn.SequentialRNNCell()
    stacked_rnn_cells.add(mx.rnn.BidirectionalCell(
                              mx.rnn.LSTMCell(num_hidden=blstm_0, prefix="lstm_0_f"),
                              mx.rnn.LSTMCell(num_hidden=blstm_0, prefix="lstm_0_b")))
    
    stacked_rnn_cells.add(mx.rnn.BidirectionalCell(
                              mx.rnn.LSTMCell(num_hidden=blstm_2, prefix="lstm_2_f"),
                              mx.rnn.LSTMCell(num_hidden=blstm_2, prefix="lstm_2_b")))
 
    outputs, states = stacked_rnn_cells.unroll(length=seq_len, inputs=data, merge_outputs=True, layout="TNC")
    
    # new #
    #outputs = mx.sym.reshape(states[0], shape=(-1, 0), reverse=True)  # Shape: (T * N, hidden_size)
    
    fc2 = mx.sym.FullyConnected(outputs, name='fc2', num_hidden=feedforward_tanh_3)
   

    # The output of SequentialRNNCell is the same as that of the last layer.
    # In this case 'outputs' is the symbol 'concat6_output' of shape (batch_size, sequence_length, hidden_dim)
    # The states of the SequentialRNNCell is a list of lists, with each list
    # corresponding to the states of each of the added cells respectively.

    softmax = mx.symbol.SoftmaxOutput(data=fc2, multi_output=True, label=target, name='sm')

    return softmax

def mx_train_eval_net(iteration, seq_len, trainX, trainY, testX, testY):
    pred = build_sym(seq_len=seq_len)

    import time

    prefix = "mx_train_weights"
    if iteration == 0: 
        t1 = time.time()      
        net = mx.mod.Module(symbol=pred, data_names=['data'], label_names=['target'], context=mx.cpu())
        t2 = time.time()      
        print("create module time:", t2-t1) 
    else:
        t1 = time.time()      
        net = mx.mod.Module.load(prefix, iteration-1, label_names=['target'])
        t2 = time.time()      
        print("load module time:", t2-t1) 
    
    train_iter = mx.io.NDArrayIter(data=trainX, label=trainY,
                                   data_name="data", label_name="target",
                                   batch_size=batch_size,
                                   shuffle=True)
    test_iter = mx.io.NDArrayIter(data=testX, label=testY,
                                  data_name="data", label_name="target",
                                  batch_size=batch_size)
    net.fit(train_data=train_iter, eval_data=test_iter,
            initializer=mx.init.Xavier(rnd_type="gaussian", magnitude=1),
            optimizer=optimizer,
            optimizer_params={"learning_rate": 1E-3},
            eval_metric="mse", num_epoch=num_epochs)

    net.save_checkpoint(prefix, iteration)

    metric = mx.metric.Accuracy()
    ret = net.score(test_iter, metric)
    
    print(metric)
    logging.info(metric)

    # make predictions
    testPredict = net.predict(test_iter).asnumpy()
    mse = np.mean((testPredict - testY)**2)
    
    out = '\nMX testing mse: {}\n'.format(mse)
    logging.info(out)

    return testPredict, mse


def keras_load_model():
    model = model_from_json(open(run_prefix+'keras_model.json').read())
    model.load_weights(run_prefix+'keras_train_weights.h5')
    model.compile(optimizer=optimizer, loss='mse')
    return model

def keras_save_model(model):    
    json_string = model.to_json()
    open(run_prefix+'keras_model.json', 'w').write(json_string)
    model.save_weights(run_prefix+'keras_train_weights.h5', overwrite=True)


class RawSeqGenerator(Callback):

    def __init__(self, train_files, val_files):
        
        self.train_files = train_files
        self.val_files = val_files
        print("train_files len:", len(self.train_files))
        print("val_files len:", len(self.val_files))

    def get_batch(self, file_in):
            sys.stdout.flush() 
            
            X, Y_num, labels, kmers = Wraph5py.get_training_data(file_in, kmer_len=kmer_len)
            Y = np_utils.to_categorical(np.array(Y_num), num_classes)
        
            # very important. It does not work without it.
            scaler = MinMaxScaler(feature_range=(0, 1))
            X = scaler.fit_transform(X)
            Y = scaler.fit_transform(Y)

            data_size = len(X) # num rows
            features = len(X[0])

            B = []
            for i in range(data_size-time_steps):
                B.append( X[i:i+time_steps] )
            XN = np.array(B)
            
            Y = Y[0:len(XN)]

            return XN, Y

    def next_train(self):
        while 1:
            random.shuffle(self.train_files) 
            for file_in in self.train_files:
                X, Y = self.get_batch(file_in)    
                indices = range(0, X.shape[0], batch_size)
                for i in indices:
                    yield (X[i:i+batch_size], Y[i:i+batch_size])

    def next_val(self):
        while 1:
            random.shuffle(self.val_files) 
            for file_in in self.val_files:
                X, Y = self.get_batch(file_in)    
                indices = range(0, X.shape[0], batch_size)
                for i in indices:
                    yield (X[i:i+batch_size], Y[i:i+batch_size])


# Create generator that yields (current features X, current labels y)
def BatchGenerator(files, train_data=True):
    while 1: 
        random.shuffle(files) 
        
        for file_in in files:
        
            sys.stdout.flush() 
            
            X, Y_num, labels, kmers = Wraph5py.get_training_data(file_in, kmer_len=kmer_len)
            Y_num -= 1
            Y = np_utils.to_categorical(np.array(Y_num), num_classes)

            # very important. It does not work without it.
            scaler = MinMaxScaler(feature_range=(0, 1))
            X = scaler.fit_transform(X)
            Y = scaler.fit_transform(Y)

            data_size = len(X) # num rows
            features = len(X[0])

            B = []
            for i in range(data_size-time_steps):
                B.append( X[i:i+time_steps] )
            XN = np.array(B)
            Y = Y[0:len(XN)]

            # split to train and testing
            train_end = int(len(Y) * 0.6)
            val_end = int(len(Y) * 0.2) + train_end
        
            # https://stats.stackexchange.com/questions/19048/what-is-the-difference-between-test-set-and-validation-set 
            trainX, valX, testX = np.array(XN[0:train_end]), np.array(XN[train_end:val_end]), np.array(XN[val_end:])
            trainY, valY, testY = np.array(Y[0:train_end]), np.array(Y[train_end:val_end]), np.array(Y[val_end:])
     
            if train_data:
                dataX, dataY = trainX, trainY
            else:
                dataX, dataY = valX, valY
            
            indices = range(0, dataX.shape[0], batch_size)

            for i in indices:
                yield (dataX[i:i+batch_size], dataY[i:i+batch_size])


def keras_gen_train_eval_net(epochs, train_files, val_files, train_steps, val_steps):
    model = Sequential()
    use_lstm = False
    if use_lstm: 
        model.add( Bidirectional(LSTM(blstm_0, 
                                      return_sequences=True, 
                                      dropout=dropout, 
                                      recurrent_dropout=recurrent_dropout ), 
                                      input_shape=[time_steps,features]))
    else:
        model.add(GRU(blstm_0, input_shape=(time_steps, features), return_sequences=True, go_backwards=True))
        model.add(GRU(blstm_0, return_sequences=True))
    
    #model.add(TimeDistributed(Dense(feedforward_tanh_1, activation='tanh'))) #causes OOM
    model.add(Dense(feedforward_tanh_1, activation='tanh'))
    if use_lstm: 
        model.add(Bidirectional(LSTM(blstm_2, dropout=dropout, recurrent_dropout=recurrent_dropout ))) 
    else:
        model.add(GRU(blstm_2, return_sequences=True, go_backwards=True))
        model.add(GRU(blstm_2, return_sequences=False))

    model.add(Dense(feedforward_tanh_3, activation='tanh')) 
    model.add(Dense(num_classes, activation="softmax"))
    model.compile(loss="categorical_crossentropy", optimizer=optimizer, metrics=['accuracy'])

    #https://github.com/fchollet/keras/issues/898
    data_gen = RawSeqGenerator(train_files, val_files)

    callbacks = [
        #ValCallback(validation_data),
        #lr_decay,  
        EarlyStopping(monitor='val_loss', patience=50, verbose=1),
        ModelCheckpoint(filepath=run_prefix+"weights.hdf5", monitor='val_loss', save_best_only=True, verbose=1),
    ]
   
    model.fit_generator(data_gen.next_train(),
                        #steps_per_epoch=int(len(files_in)*0.5),
                        steps_per_epoch=train_steps,
                        epochs=epochs,
                        verbose=1,
                        validation_data=data_gen.next_val(),                       
                        #validation_steps=int(len(files_in)*0.5),
                        validation_steps=val_steps,
                        callbacks=callbacks
                        ) 
     
    logging.info("saving model...")
    keras_save_model(model)
    logging.info("completed")
    

def keras_run_pred(file_in):

    model = keras_load_model()

    logging.info("basecalling " + file_in)

    X, Y_num, labels, kmers = Wraph5py.get_training_data(file_in, kmer_len=kmer_len)
    Y = np_utils.to_categorical(np.array(Y_num), num_classes)

    # very important. It does not work without it.
    scaler = MinMaxScaler(feature_range=(0, 1))
    X = scaler.fit_transform(X)
    Y = scaler.fit_transform(Y)

    data_size = len(X) # num rows
    seq_len = 1 
    data_dim = len(X[0])

    X = X.reshape(data_size, seq_len, data_dim) 

    post = model.predict( X, batch_size=batch_size, verbose=0 )

    logging.info("posterior generated")

    return post


# BiRNN Model (Many-to-One)
class BiRNN(nn.Module):
    def __init__(self, input_size, hidden_size, num_layers, num_classes):
        super(BiRNN, self).__init__()
        self.hidden_size = hidden_size
        self.num_layers = num_layers
        self.lstm = nn.LSTM(input_size, hidden_size, num_layers, batch_first=False, bidirectional=True)
        self.fc = nn.Linear(hidden_size*2, num_classes)  # 2 for bidirection 

    def forward(self, x):
        # Set initial states
        h0 = Variable(torch.zeros(self.num_layers*2, x.size(0), self.hidden_size)) # 2 for bidirection 
        c0 = Variable(torch.zeros(self.num_layers*2, x.size(0), self.hidden_size))

        # Forward propagate RNN
        out, _ = self.lstm(x, (h0, c0))

        # Decode hidden state of last time step
        #out = self.fc(out[:, -1, :])

        all_rows_out = self.fc(out)
        return all_rows_out


class CNN(nn.Module):
    def __init__(self):
        super(CNN, self).__init__()
        self.layer1 = nn.Sequential(
            nn.Conv2d(1, 16, kernel_size=5, padding=2),
            nn.BatchNorm2d(16),
            nn.ReLU(),
            nn.MaxPool2d(2))
        self.layer2 = nn.Sequential(
            nn.Conv2d(16, 32, kernel_size=5, padding=2),
            nn.BatchNorm2d(32),
            nn.ReLU(),
            nn.MaxPool2d(2))
        self.fc = nn.Linear(7*7*32, 10)
        
    def forward(self, x):
        out = self.layer1(x)
        out = self.layer2(out)
        out = out.view(out.size(0), -1)
        out = self.fc(out)
        return out


def pytorch_train_eval_net(iteration, trainX, trainY, valX, valY):

    #print("run pytorch_train_eval_net...")

    # train
    output_seq = rnn(trainX)
    last_output = output_seq[-1]

    # (num_rows x num_classes), (num_rows, )
    
    #TODO 
    err = loss(last_output, trainY) # err AKA cost
    err.backward()
    optimizer.step()

    prediction = torch.max(softmax(last_output), 1)[1]
    correct_prediction = (prediction.data == trainY.data)
    train_loss, train_acc = err.data[0], correct_prediction.float().mean()
  
    # val
    output_seq = rnn(valX)
    last_output = output_seq[-1]

    err = loss(last_output, valY) # err AKA cost

    post = softmax(last_output)

    prediction = torch.max(post, 1)[1]
    correct_prediction = (prediction.data == valY.data)
    val_loss, val_acc = err.data[0], correct_prediction.float().mean()

    if iteration % 200 == 0:
        out = '\nPyTorch train_loss: {}, train_acc: {}, val_loss: {}, val_acc: {}\n'.format(train_loss, train_acc, val_loss, val_acc)
        print(out)
        logging.info(out)

    return post.data.numpy()


def post_to_seq(post, kmers):

    min_prob = 1e-5
    # Manipulate posterior matrix
    post, good_events = PostUtils.clean_post(post, kmers, min_prob)

    trans = None
    # fast_estimate_transitions
    trans = PostUtils.estimate_transitions(post, trans=trans) #rs_mod [[stay1, step1, skip1], ..... num events]

    score, states = PostUtils.decode_profile(post, trans=np.log(__ETA__ + trans), log=False)

    # Form basecall
    kmers = [x for x in kmers if 'X' not in x]
    qdata = SeqUtils.get_qdata(post, kmers)

    qscore_correction = 'template' 
    seq, qual, kmer_path = SeqUtils.form_basecall(qdata, kmers, states, qscore_correction=qscore_correction)

    #rtn_value = [(fname, (seq, qual), score, len(features)), (network_time, decode_time)]

    return seq, qual, kmer_path


def print_save_seq(seq, file_in):

    print(file_in)
    w, i = 80, -1
    for i in range(len(seq) // w):
        print( seq[i*w:(i+1)*w] )
    print( seq[(i+1)*w:] )

    with open(file_in+".fasta", "a") as f:
        f.write(file_in+"\n")

        w, i = 80, -1
        for i in range(len(seq) // w):
            f.write(seq[i*w:(i+1)*w]+"\n")
        f.write(seq[(i+1)*w:]+"\n")


def run_training(iteration, file_in, kmer_len):

    X, Y_num, labels, kmers = Wraph5py.get_training_data(file_in, kmer_len=kmer_len)
    Y = np_utils.to_categorical(np.array(Y_num), num_classes)

    # very important. It does not work without it.
    scaler = MinMaxScaler(feature_range=(0, 1))
    X = scaler.fit_transform(X)
    Y = scaler.fit_transform(Y)

    data_size = len(X) # num rows
    seq_len = 1 
    data_dim = len(X[0])
    
    X = X.reshape(data_size, seq_len, data_dim) #TODO

    # split to train and testing
    train_end = int(len(Y) * 0.6)
    val_end = int(len(Y) * 0.2) + train_end
   
    # https://stats.stackexchange.com/questions/19048/what-is-the-difference-between-test-set-and-validation-set 
    trainX, valX, testX = np.array(X[0:train_end]), np.array(X[train_end:val_end]), np.array(X[val_end:])
    trainY, valY, testY = np.array(Y[0:train_end]), np.array(Y[train_end:val_end]), np.array(Y[val_end:])

    print("Begin to train LSTM without CUDNN acceleration...")
    begin = time.time()
    post, normal_mse = mx_train_eval_net(iteration, 1, trainX, trainY, testX, testY)

    end = time.time()
    normal_time_spent = end - begin
    print("Done!")
   
    #print("CUDNN time spent: %g, test mse: %g" % (cudnn_time_spent, cudnn_mse))
    print("NoCUDNN time spent: %g, test mse: %g" % (normal_time_spent, normal_mse))
   
    plot = False
    if plot:
        plt.close('all')
        fig = plt.figure()
        plt.plot(testY, label='Groud Truth')
        #plt.plot(cudnn_pred, label='With cuDNN')
        plt.plot(post, label='Without cuDNN')
        plt.legend()
        plt.show()

    pred_seq, pred_qual, pred_kmer_path = post_to_seq(post, kmers)
    test_seq, test_qual, test_kmer_path = post_to_seq(testY, kmers)

    file_in = os.path.split(file_in)[-1][:-6]

    print("pred_seq:")
    print_save_seq(pred_seq, run_prefix + "PRED_" + file_in)

    print("test_seq:")
    print_save_seq(test_seq, run_prefix + "TEST_" + file_in)


def pytorch_run_training(iteration, file_in, kmer_len):

    #print("pytorch_run_training, iteration:", iteration)

    X, Y, labels, kmers = Wraph5py.get_training_data(file_in, kmer_len=kmer_len)
    #Y = np_utils.to_categorical(np.array(Y_num), num_classes)

    Y = Y.astype(np.int64)

    # very important. It does not work without it.
    scaler = MinMaxScaler(feature_range=(0, 1))
    X = scaler.fit_transform(X)
    #Y = scaler.fit_transform(Y)

    data_size = len(X) # num rows
    seq_len = 1 
    features = len(X[0])
    
    # split to train and testing
    train_end = int(len(Y) * 0.8)
   
    trainX, valX = np.array(X[0:train_end]).astype(np.float32), np.array(X[train_end:]).astype(np.float32)
    trainY, valY = np.array(Y[0:train_end]), np.array(Y[train_end:])

    # py_torch ordering (time_steps, batch_size, features)
    trainX = trainX.reshape(seq_len, -1, features)
    valX = valX.reshape(seq_len, -1, features)
    trainY = trainY.reshape(-1,)
    valY = valY.reshape(-1,)

    trainX = Variable(torch.from_numpy(trainX))
    trainY = Variable(torch.from_numpy(trainY))
    valX = Variable(torch.from_numpy(valX))
    valY = Variable(torch.from_numpy(valY))

    begin = time.time()
    post = pytorch_train_eval_net(iteration, trainX, trainY, valX, valY)

    end = time.time()
    normal_time_spent = end - begin

    file_in = os.path.split(file_in)[-1][:-6]
   
    '''
    pred_seq, pred_qual, pred_kmer_path = post_to_seq(post, kmers)
    print("pred_seq:")
    print_save_seq(pred_seq, run_prefix + "PRED_" + file_in)

    #TODO valY to categorical
    #val_seq, val_qual, val_kmer_path = post_to_seq(valY, kmers)
    #print("val_seq:")
    #print_save_seq(val_seq, run_prefix + "TEST_" + file_in)
    '''

if __name__ == "__main__":

    parser = argparse.ArgumentParser(description='basecaller mode and options')
    parser.add_argument('--epochs', type=int, default=500)
    parser.add_argument('--kmer_len', type=int, default=4)
    parser.add_argument('--lib',
                        default='pytorch',
                        choices=['keras', 'mxnet', 'pytorch'],
                        help='library (default: %(default)s)')
    parser.add_argument('--mode',
                        default='train',
                        choices=['train', 'call'],
                        help='mode (default: %(default)s)')
    parser.add_argument('--data',
                        help='data directory')

    args = parser.parse_args()
    
    print( args.epochs )
    print( args.lib )
    print( args.mode )
    print( args.data )

    path = args.data
    files = os.listdir(path)
    if args.lib == "keras":
        files = [path+f for f in files]

        train_size, val_size = 0, 0
        train_files = files[0:int(len(files)*train_ratio)]
        val_files = files[len(train_files):]

        train_size = sum([Wraph5py.get_training_size(file_in) for file_in in train_files])
        val_size = sum([Wraph5py.get_training_size(file_in) for file_in in val_files])

        train_steps = int(train_size / batch_size)
        val_steps = int(val_size / batch_size)
    
        print("train_size:", train_size, "train_steps:", train_steps)
        print("val_size:", val_size, "val_steps:", val_steps)

        if args.mode == "train":
            keras_gen_train_eval_net(args.epochs, train_files, val_files, train_steps, val_steps)
        else:  
            for f in files:
                post = keras_run_pred(f)
               
                kmers = Wraph5py.get_kmers(kmer_len, alpha=alphabet)

                pred_seq, pred_qual, pred_kmer_path = post_to_seq(post, kmers)
                file_in = os.path.split(f)[-1][:-6]
                print_save_seq(pred_seq, "PRED_" + file_in)

    elif args.lib == "mxnet":
        print("files:")
        for i, seqf in enumerate(files):
            f = path + seqf
            print("running file no. ", i, seqf)
            run_training(i, f, kmer_len)
    else:
        print("files:")

        hidden_size = 100
        num_layers = 2

        rnn = BiRNN(features, hidden_size, num_layers, num_classes)

        softmax = torch.nn.Softmax()
    
        loss = torch.nn.CrossEntropyLoss() # loss AKA criterion
        
        optimizer = torch.optim.Adam(rnn.parameters(), lr=learning_rate)

        for e in range(args.epochs):
            random.shuffle(files) 
            for i, seqf in enumerate(files):
                f = path + seqf
                #print("running file no. ", i, "of", len(files), seqf)
                pytorch_run_training(i + e*len(files), f, kmer_len)

