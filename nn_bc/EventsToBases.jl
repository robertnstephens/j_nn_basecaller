############################################
# author: Robert Stephens                  #
# email:  robert.stephens@nanoporetech.com #
############################################

module EventsToBases

using Knet, AutoGrad, ArgParse, Compat

using Features, Network, Config, Decoder, GenUtils, FormBasecall

export events_to_bases
function events_to_bases(events)

    println("run events_to_bases...")

    println("old length events: " * string(length(events)))

    events, features, features_arr = events_to_features(events, use_trim=true)

    #TODO from JSON config file 
    #min_len, max_len = 500, 50000
    #if length(events) < min_len || length(events) > max_len
    #    println("read length " * string(length(events)) * " not supported")
    #    return -1
    #end
    
    @time post = Network.run(events, features_arr) #TODO: heavy lifting with KNet

    kmers = gen_kmers() 
    
    #this gets rid of column 1025 after excluding likely XXXXX rows
    post, good_events = clean_post(post, kmers)
    if post == nothing
        return nothing
    end


    @time trans = fast_estimate_transitions(post) #rs_mod [[stay1, step1, skip1], ..... num events]

    println("trans m*n:")
    println(size(trans, 1))
    println(size(trans, 2))

    @time score, states = run_decoder(post, trans)

    # Form basecall
    @time qdata = get_qdata(post, kmers) #num events * (_KMER_LEN*_BASE_LEN)

    @time seq, qual, kmer_path = form_basecall(qdata, kmers, states)

    return seq

end


function clean_post(post, kmers; min_prob=1e-5)
    # Do we have an XXX kmer? Strip out events where XXX most likely, and XXX states entirely
    
    good_event_inds = []
    if kmers[end] == join(["X" for _ in 1:length(kmers[end])])
        bad_kmer = size(post,2)
        
        max_call_val, max_call = findmax(post, 2) 
        max_call = ind2sub(size(post), vec(max_call))[2]
    
        #TODO better way
        good_events = (max_call .!= bad_kmer)
        for (i, good_event) in enumerate(good_events)
            if good_event
                push!(good_event_inds, i)
            end
        end 
        
        println("post size before:")  
        println(size(post,1)) 
        println(size(post,2)) 

        post = post[good_event_inds,:]       

        println("good_event_inds:")
        println(size(good_event_inds,1))
        println("post size after:")  
        println(size(post,1)) 

        post = post[:,1:end-1]
        
        if size(post,1) == 0
            return nothing, nothing
        end
    end

    weights = sum(post, 2)   
    post = post ./ weights
    
    post = min_prob + (1.0 - min_prob) * post
    return post, good_event_inds
end


end #module EventsToBases

