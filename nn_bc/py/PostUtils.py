def clean_post(post, kmers, min_prob):
    # Do we have an XXX kmer? Strip out events where XXX most likely,
    #    and XXX states entirely

    if kmers[-1] == 'X'*len(kmers[-1]):
        bad_kmer = post.shape[1] - 1
        max_call = np.argmax(post, axis=1)
        good_events = (max_call != bad_kmer)
        post = post[good_events]
        post = post[:, :-1]
        if len(post) == 0:
            return None, None

    weights = np.sum(post, axis=1).reshape((-1,1))
    post /= weights
    post = min_prob + (1.0 - min_prob) * post
    return post, good_events

import itertools
import numpy as np
import os

from numpy.ctypeslib import ndpointer
from ctypes import c_double, c_size_t

#from nanonet import cl
#from nanonet.nn import dtype, tiny, build_program
#from nanonet.util import get_shared_lib


_ETA = np.finfo(np.float32).tiny
_BASES = ['A', 'C', 'G', 'T']
_DIBASES = [b1 + b2 for b1 in _BASES for b2 in _BASES]
_NSTEP = len(_BASES)
_NSKIP = _NSTEP ** 2
_STEP_FACTOR = np.log(_NSTEP)
_SKIP_FACTOR = np.log(_NSKIP)


def decode_profile(post, trans=None, log=False, slip=0.0):
    """  Viterbi-style decoding with per-event transition weights
    (profile)
    :param post: posterior probabilities of kmers by event.
    :param trans: A generator (e.g. a :class:`ndarray`) to produce
    per-transition log-scaled weights. None == no transition weights.
    :param log: Posterior probabilities are in log-space.
    """
    
    nstate = post.shape[1] #rs_mod 1024 size array, post.shape[0] == num events

    #rs_mod
    #import pdb
    #pdb.set_trace()

    lpost = post.copy()
    if not log: #rs_mod ???????????????????
        np.add(_ETA, lpost, lpost) 
        np.log(lpost, lpost)       #rs_mod ln() - calling with two arrays mutates
    
    if trans is None:
        trans = itertools.repeat(np.zeros(3))
    else:
        trans = np.copy(trans) #rs_mod trans is [[stay1, step1, skip1], [stay2, step2, skip2], .... num events]
        trans[:,1] -= _STEP_FACTOR #rs_mod -= ln(4)
        trans[:,2] -= _SKIP_FACTOR #rs_mod -= ln(16)

    log_slip = np.log(_ETA + slip) 


    #rs_mod
    #f_handle = open("lpost_row_sums.csv",'a')
    #f_handle.write("\n######### lpost sums ############\n")
    #np.savetxt(f_handle, np.array( [sum(lpost[0]), sum(lpost[10]), sum(lpost[20])] ), delimiter=',')
    #f_handle.write("\n######### end lpost sums ############\n")

    #rs_mod
    #f_handle = open("before_lpost.csv",'a')
    #f_handle.write("\n######### lpost ############\n")
    #np.savetxt(f_handle, lpost, delimiter=',')
    #f_handle.write("\n######### end lpost ############\n")


    pscore = lpost[0]
    trans_iter = trans.__iter__()
    for ev in range(1, len(post)):
        # Forward Viterbi iteration
        #rs_mod 
        #ev_trans = trans_iter.next()
        ev_trans = next(trans_iter)
        
        # Stay
        score = pscore + ev_trans[0]
        iscore = range(nstate) #rs_mod [0, 1 ... 1023] TODO
        # Slip
        scoreNew = np.amax(pscore) + log_slip #rs_mod max array value
        iscoreNew = np.argmax(pscore)         #rs_mod max array value index
        iscore = np.where(score > scoreNew, iscore, iscoreNew)
        score = np.fmax(score, scoreNew)      #rs_mod element-wise max of array elements
        # Step
        #TODO: ValueError: cannot reshape array of size 63 into shape (4,newaxis)
        pscore = pscore.reshape((_NSTEP, -1)) #rs_mod 4*256
        nrem = pscore.shape[1]                #rs_mod 256
        scoreNew = np.repeat(np.amax(pscore, axis=0), _NSTEP) + ev_trans[1] #rs_mod 256 maxes repeated 4 times
        iscoreNew = np.repeat(nrem * np.argmax(pscore, axis=0) + range(nrem), _NSTEP)
        iscore = np.where(score > scoreNew, iscore, iscoreNew)
        score = np.fmax(score, scoreNew)
        # Skip
        pscore = pscore.reshape((_NSKIP, -1)) #rs_mod 16*64
        nrem = pscore.shape[1]                #rs_mod 64
        scoreNew = np.repeat(np.amax(pscore, axis=0), _NSKIP) + ev_trans[2]
        iscoreNew = np.repeat(nrem * np.argmax(pscore, axis=0) + range(nrem), _NSKIP)
        iscore = np.where(score > scoreNew, iscore, iscoreNew)
        score = np.fmax(score, scoreNew)
        # Store
        lpost[ev-1] = iscore
        pscore = score + lpost[ev]

    state_seq = np.zeros(len(post), dtype=int)
    state_seq[-1] = np.argmax(pscore)
    for ev in range(len(post), 1, -1):
        # Viterbi backtrace
        state_seq[ev-2] = int(lpost[ev-2][state_seq[ev-1]])
       
        #logger.info("rs_mod: " + str(state_seq[ev-2]) ) 
        #print "rs_mod:", str(state_seq[ev-2]) #very verbose

    #rs_mod
    #f_handle = open("lpost.csv",'a')
    #f_handle.write("\n######### lpost ############\n")
    #np.savetxt(f_handle, lpost, delimiter=',')
    #f_handle.write("\n######### end lpost ############\n")

    return np.amax(pscore), state_seq
    

def decode_transition(post, trans, log=False, slip=0.0):
    """  Viterbi-style decoding with weighted transitions
    :param post: posterior probabilities of kmers by event.
    :param trans: (log) penalty for [stay, step, skip]
    :param log: Posterior probabilities are in log-space.
    """
    return decode_profile(post, trans=itertools.repeat(trans), log=log, slip=slip)


def decode_simple(post, log=False, slip=0.0):
    """  Viterbi-style decoding with uniform transitions
    :param post: posterior probabilities of kmers by event.
    :param log: Posterior probabilities are in log-space.
    """
    return decode_profile(post, log=log, slip=slip)


def decode_homogenous(post, log=False, n_bases=4):
    if not log:
        post = np.log(_ETA + post)
    post = post.astype(np.float32)

    func = nanonetdecode.decode_path
    func.restype = np.float32
    func.argtypes = [ndpointer(dtype=np.float32, flags='CONTIGUOUS'),
                     c_size_t, c_size_t, c_size_t]
    score = func(post, post.shape[0], n_bases, post.shape[1])
    states = post[:,0].astype(int)
    return score, states


def estimate_transitions(post, trans=None):
    """  Naive estimate of transition behaviour from posteriors
    :param post: posterior probabilities of kmers by event.
    :param trans: prior belief of transition behaviour (None = use global estimate)
    """
    assert trans is None or len(trans) == 3, 'Incorrect number of transitions'
    res = np.zeros((len(post), 3)) #res num events * 3
    res[:] = _ETA #rs_mod 1e-30F

    for ev in range(1, len(post)): #rs_mod iterate over all events
        stay = np.sum(post[ev-1] * post[ev]) #rs_mod sum 1025 elements
        p = post[ev].reshape((-1, _NSTEP)) #rs_mod 4, so reshaped as 4*256 -> 4 possible choices if stepping
        step = np.sum(post[ev-1] * np.tile(np.sum(p, axis=1), _NSTEP)) / _NSTEP #(np.sum(p, axis=1) is 256, tile 4 times = 1024 array 
        p = post[ev].reshape((-1, _NSKIP)) #rs_mod 16, so reshaped as 16*64 -> 16 possible choices if skipping
        skip = np.sum(post[ev-1] * np.tile(np.sum(p, axis=1), _NSKIP)) / _NSKIP
        res[ev-1] = [stay, step, skip]

    if trans is None:
        trans = np.sum(res, axis=0)
        trans /= np.sum(trans)

    res *= trans
    res /= np.sum(res, axis=1).reshape((-1,1))

    return res

