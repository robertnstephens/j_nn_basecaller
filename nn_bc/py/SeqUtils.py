import argparse
import json
import os
import re
import math
import sys
import shutil
import tempfile
import timeit
import subprocess
import pkg_resources
import itertools
import numpy as np
from functools import partial
from itertools import tee

__ETA__ = np.finfo(np.float32).tiny


def get_qdata(post, kmers):
    bases = sorted(set(''.join(kmers)))
    kmer_len = len(kmers[0])
    n_events = len(post)
    n_bases = len(bases)

    qdata = np.empty((n_events, len(bases)*kmer_len), dtype=post.dtype)
    for i, (pos, base) in enumerate(itertools.product(range(kmer_len), bases)):
        cols = np.fromiter((k[pos] == base for k in kmers),
            dtype=bool, count=len(kmers))
        
        qdata[:, i] = np.sum(post[:, cols], axis=1)
    return qdata


def form_basecall(qdata, kmers, states, qscore_correction=None):
    bases = sorted(set(''.join(kmers)))
    kmer_len = len(kmers[0])
    n_events = len(qdata)
    n_bases = len(bases)
    kmer_path = [kmers[i] for i in states] 

    # e.g. 0 for AACT, AACT; 1 for TAAC, AACA; 2 for TAAC, ACGA; 
    moves = kmer_overlap(kmer_path) 
    seq_len = np.sum(moves) + kmer_len 
    scores = np.zeros((seq_len, len(bases)), dtype=np.float32)
    sequence = list(kmer_path[0])
    posmap = list(range(kmer_len))

    _contribute(scores, qdata[0, :], posmap, n_bases)
    for event, move in enumerate(moves):
        if move > 0:
            if move == kmer_len:
                posmap = list(range(posmap[-1] + 1, posmap[-1] + 1 + kmer_len))
            else:
                posmap[:-move] = posmap[move:] #[2, 3, 4, 3, 4] if move == 2
                #[2, 3, 4, 5, 6] if move == 2, [3, 4, 5, 6, 7] if next move 1
                posmap[-move:] = list(range(posmap[-move-1] + 1, posmap[-move-1] + 1 + move))
            sequence.append(kmer_path[event][-move:])
        _contribute(scores, qdata[event, :], posmap, n_bases)
    sequence = ''.join(sequence)
    base_to_pos = {b:i for i,b in enumerate(bases)}
    scores += __ETA__
    scoresums = np.sum(scores, axis=1)
    scores /= scoresums[:, None]
    called_probs = np.fromiter(
        (scores[n, base_to_pos[base]] for n, base in enumerate(sequence)),
        dtype=float, count=len(sequence)
    )

    if qscore_correction == 'template':
        # initial scores fit to empirically observed probabilities
        #   per score using: Perror = a.10^(-bQ/10). There's a change
        #   in behaviour at Q10 so we fit two lines. (Q10 seems suspicious).
        switch_q = 10
        a, b = 0.05524, 0.70268
        c, d = 0.20938, 1.00776
        switch_p = 1.0 - np.power(10.0, - 0.1 * switch_q)
        scores = np.empty_like(called_probs)
        for x, y, indices in zip((a,c), (b,d), (called_probs < switch_p, called_probs >= switch_p)):
            scores[indices] = -(10.0 / np.log(10.0)) * (y*np.log1p(-called_probs[indices]) + np.log(x))
    elif qscore_correction in ('2d','complement'):
        # same fitting as above
        if qscore_correction == 'complement':
            x, y = 0.13120, 0.88952
        else:
            x, y = 0.02657, 0.65590 
        scores = -(10.0 / np.log(10.0)) * (y*np.log1p(-called_probs) + np.log(x))
    else:
        scores = -10.0 * np.log1p(-called_probs) / np.log(10.0)

    offset = 33
    scores = np.clip(np.rint(scores, scores).astype(int) + offset, offset, 126)
    qstring = ''.join(chr(x) for x in scores)
    return sequence, qstring, kmer_path


def _contribute(scores, qdata, posmap, n_bases):
     kmerlen = len(posmap)
     for kmer_pos, seq_pos in enumerate(posmap):
         index = (kmerlen - kmer_pos - 1) * n_bases
         scores[seq_pos, :] += qdata[index:index + n_bases]
     return


def kmer_overlap(kmers, moves=None, it=False):
    """From a list of kmers return the character shifts between them.
    (Movement from i to i+1 entry, e.g. [AATC,ATCG] returns [0,1]).

    :param kmers: sequence of kmer strings.
    :param moves: allowed movements, if None all movements to length of kmer
        are allowed.
    :param it: yield values instead of returning a list.

    Allowed moves may be specified in moves argument in order of preference.
    """

    if it:
        return kmer_overlap_gen(kmers, moves)
    else:
        return list(kmer_overlap_gen(kmers, moves))


def kmer_overlap_gen(kmers, moves=None):
    """From a list of kmers return the character shifts between them.
    (Movement from i to i+1 entry, e.g. [AATC,ATCG] returns [0,1]).
    Allowed moves may be specified in moves argument in order of preference.

    :param moves: allowed movements, if None all movements to length of kmer
        are allowed.
    """

    first = True
    yield 0
    for last_kmer, this_kmer in window(kmers, 2):
        if first:
            if moves is None:
                l = len(this_kmer)
                moves = range(l + 1)
            first = False

        l = len(this_kmer)
        for j in moves:
            if j < 0:
                if last_kmer[:j] == this_kmer[-j:]:
                    yield j
                    break
            elif j > 0 and j < l:
                if last_kmer[j:l] == this_kmer[0:-j]:
                    yield j
                    break
            elif j == 0:
                if last_kmer == this_kmer:
                    yield 0
                    break
            else:
                yield l
                break


def window(iterable, size):
    """Create an iterator returning a sliding window from another iterator.

    :param iterable: iterable object.
    :param size: size of window.
    """

    iters = tee(iterable, size)
    for i in range(1, size):
        for each in iters[i:]:
            next(each, None)
    return zip(*iters)
