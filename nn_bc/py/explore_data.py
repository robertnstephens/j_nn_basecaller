#! /usr/bin/env python2

from __future__ import print_function

import sys, os
import Wraph5py

'''
p = "/home/zen/training_data/one_file/"
f = "MINICOL607_20170428_FNFAE33183_MN20031_sequencing_throughput_AMW_model_training_R9_5_1_1_dev_19071_ch9_read7777_strand.fast5" 
filename = p+f
event_path = "/Analyses/AlignToRef_000/CurrentSpaceMapped_template/Events/"
print(filename)
'''

#labels = Wraph5py.get_labels(filename, event_path)

#print(labels[-1])

#filename = sys.argv[1]

path = sys.argv[1]

files = os.listdir(path)

for f in files:
    f = path + f 

    X, y, labels = Wraph5py.get_training_data(f)
   
    print( type(X), type(y), type(labels) )
    
    print( X[0] )
    print( y[0] )
    print( labels[0] )
    
    break
