import sys, traceback
from itertools import product

from fast5 import Fast5

import h5py
import numpy as np
import numpy.lib.recfunctions as nprf

#TODO
window=[-1, 0, 1]
#kmer_len=3
alphabet='ACGT'
chunk_size=1000
min_chunk=900
trim=10
#get_events=get_events_ont_mapping
#get_labels=get_labels_ont_mapping
#callback_kwargs={'section':'template', 'kmer_len':kmer_len}



def get_training_size(f, trim=10, section='template'):
    
    with Fast5(f) as fh:
        size = fh.get_mapping_size(section=section)
    return size - trim*2

def get_training_data(f, kmer_len=3, trim=10):
   
    callback_kwargs={'section':'template', 'kmer_len':kmer_len}
    
    kmers = get_kmers(kmer_len, alpha=alphabet) 
    
    all_kmers = {k:i for i,k in enumerate(kmers)}


    try: # lot of stuff
        # Run callbacks to get features and labels
        X = events_to_features(get_events(f, **callback_kwargs), window=window)
        labels = get_labels(f, **callback_kwargs)
    except:
        print("Skipping: {}, features/labels reading exception".format(f))
       
        exc_type, exc_value, exc_traceback = sys.exc_info()
        lines = traceback.format_exception(exc_type, exc_value, exc_traceback)
        print("".join('!! ' + line for line in lines))  # Log it or whatever here


        #TODO: all failing here!  
        #continue
        return None
    try:
        X = X[trim:-trim]
        labels = labels[trim:-trim]
       
        #rs_mod
        labels = [label.decode("utf-8") for label in labels] 
        
        if len(X) != len(labels):
            raise RuntimeError('Length of features and labels not equal.')
    except:
        print("Skipping: {}, features/labels length mismatch".format(f))

    try:
        # convert kmers to ints
        y = np.fromiter(
            (all_kmers[k] for k in labels),
            dtype=np.int16, count=len(labels)
        )
    except Exception as e:
        # Checks for erroneous alphabet or kmer length
        raise RuntimeError(
            'Could not convert kmer labels to ints in file {}. '
            'Check labels are no longer than {} and contain only {}'.format(f, kmer_len, alphabet)
        )

    return X, y, labels, kmers


def get_labels(filename, kmer_len=3, section='template'):
    """Scrape kmer labels from .fast5 file.

    :param filename: input file.
    :param kmer_len: length of kmers to return as labels.
    :param section: template or complement

    """
    bad_kmer = 'X'*kmer_len
    with Fast5(filename) as fh:
        # just get template mapping data
        events, _ = fh.get_any_mapping_data(section=section)
        base_kmer_len = len(events['kmer'][0])
        if base_kmer_len < kmer_len:
            raise ValueError(
                'kmers in mapping file are {}mers, but requested {}mers.'.format(
                base_kmer_len, kmer_len
            ))
        if base_kmer_len == kmer_len:
            y = events['kmer']
        else:
            #k1 = base_kmer_len//2 - kmer_len//2 - 1
            k1 = 0 
            k2 = k1 + kmer_len
            y = np.fromiter(
                #rs_mod 
                #(k[k1:k2] for k in events['kmer']),
                (k[int(k1):int(k2)] for k in events['kmer']),
                dtype='>S{}'.format(kmer_len),
                count = len(events)
            )
        y[~events['good_emission']] = bad_kmer
    return y


def get_events(filename, kmer_len=3, section='template'):
    """Scrape event-alignment data from .fast5
    
    :param filename: input file.
    :param section: template or complement
    """
    with Fast5(filename) as fh:
        events, _ = fh.get_any_mapping_data(section=section)
    return events


def test_get_labels(fast5_file, event_path):
    #print event_path, "\n", fast5_file
    with h5py.File(fast5_file, 'r') as input_fast5:
    
        events = input_fast5[event_path]
        
        labels = [] 
        for event in events:
            labels.append(event[-2][:-1])
        
        return labels

def events_to_features(events, window=[-1, 0, 1], sloika_model=False):
    """Read events from a .fast5 and return feature vectors.

    :param filename: path of file to read.
    :param window: list specifying event offset positions from which to
        derive features. A short centered window is used by default.
    """
    fg = SquiggleFeatureGenerator(events, sloika_model=sloika_model)
    for pos in window:
        fg.add_mean_pos(pos)
        fg.add_sd_pos(pos)
        fg.add_dwell_pos(pos)
        fg.add_mean_diff_pos(pos)
    X = fg.to_numpy()

    return X


class SquiggleFeatureGenerator(object):
    def __init__(self, events, labels=None, sloika_model=False):
        """Feature vector generation from events.

        :param events: standard event array.
        :param labels: labels for events, only required for training

        ..note:
            The order in which the feature adding methods is called should be the
            same for both training and basecalling.
        """
        self.events = np.copy(events)
        self.labels = labels
        self.features = {}
        self.feature_order = []

        filename = "events_out_"

        # Augment events
        if sloika_model:
            delta = np.abs(np.ediff1d(events['mean'], to_end=0))
            self.events = nprf.append_fields(events, 'delta', delta)
            for field in ('mean', 'stdv', 'length', 'delta'):
                scale_array(self.events[field], copy=False)
        else:
            for field in ('mean', 'stdv', 'length',):
                scale_array(self.events[field], copy=False)
               
            delta = np.ediff1d(self.events['mean'], to_begin=0) #diff between conseq. elements in array
            scale_array(delta, with_mean=False, copy = False)
            
            self.events = nprf.append_fields(self.events, 'delta', delta)
 
    def to_numpy(self):
        out = np.empty((len(self.events), len(self.feature_order)))
        for j, key in enumerate(self.feature_order):
            out[:, j] = self.features[key]
        return out

    def _add_field_pos(self, field, pos):
        tag = "{}[{}]".format(field, pos)
        if tag in self.features:
            return self
        self.feature_order.append(tag)
        self.features[tag] = padded_offset_array(self.events[field], pos)
        return self

    def add_mean_pos(self, pos):
        self._add_field_pos('mean', pos)
        return self

    def add_sd_pos(self, pos):
        self._add_field_pos('stdv', pos)
        return self

    def add_dwell_pos(self, pos):
        self._add_field_pos('length', pos)
        return self

    def add_mean_diff_pos(self, pos):
        self._add_field_pos('delta', pos)
        return self



def all_kmers(alphabet='ACGT', length=5, rev_map=False):
    """ Find all possible kmers of given length.

    .. Warning::
       The return value type of this function is dependent on the input
       arguments

    :param alphabet: string from which to draw characters
    :param length: length of kmers required
    :param rev_map: return also a dictionary containing the reverse mapping i.e. {'AAA':0, 'AAC':1}

    :returns: a list of strings. kmers are sorted by the ordering of the *alphabet*. If *rev_map*
        is specified a second item is returned as noted above.

    """
    fwd_map = map(lambda x: ''.join(x), product(alphabet, repeat=length))
    if not rev_map:
        return fwd_map
    else:
        return fwd_map, dict(zip(fwd_map, xrange(len(fwd_map))))


def all_nmers(n=3, alpha='ACGT'):
    return all_kmers(length=n, alphabet=alpha)

def get_kmers(kmer_len=3, alpha=alphabet):
    # Our state labels are kmers plus a junk kmer
    kmers = list(all_nmers(kmer_len, alpha=alphabet))
    bad_kmer = 'X'*kmer_len
    kmers.append(bad_kmer)
    return kmers

def padded_offset_array(array, pos):
    """Offset an array and pad with zeros.
    :param array: the array to offset.
    :param pos: offset size, positive values correspond to shifting the
       original array to the left (and padding the end of the output).
    """
    out = np.empty(len(array))
    if pos == 0:
        out = array
    elif pos > 0:
        out[:-pos] = array[pos:]
        out[-pos:] = 0.0
    else:
        out[:-pos] = 0.0
        out[-pos:] = array[0:pos]
    return out

def scale_array(X, with_mean=True, with_std=True, copy=True):
    """Standardize an array
    Center to the mean and component wise scale to unit variance.
    :param X: the data to center and scale.
    :param with_mean: center the data before scaling.
    :param with_std: scale the data to unit variance.
    :param copy: copy data (or perform in-place)
    """
    X = np.asarray(X)
    if copy:
        X = X.copy()
    if with_mean:
        mean_ = np.mean(X)
        X -= mean_
        mean_1 = X.mean()
        if not np.allclose(mean_1, 0.0):
            X -= mean_1
    if with_std:
        scale_ = np.std(X)
        if scale_ == 0.0:
            scale_ = 1.0
        X /= scale_
        if with_mean:
            mean_2 = X.mean()
            if not np.allclose(mean_2, 0.0):
                X -= mean_2
    return X

