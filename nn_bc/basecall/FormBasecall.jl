
############################################
# author: Robert Stephens                  #
# email:  robert.stephens@nanoporetech.com #
############################################

module FormBasecall

using Knet, AutoGrad, ArgParse, Compat

using Config, GenUtils

export get_qdata
function get_qdata(post, kmers)
    println("run get_qdata...") 
    
    num_events = size(post, 1)   

    qdata = zeros(num_events, _BASES_LEN*_KMER_LEN) 

    i = 1
    for pos in 1:_KMER_LEN, base in _BASES
        cols = []
        for (j, kmer) in enumerate(kmers)
            if string(kmer[pos]) == base
                push!(cols, j)
            end 
        end
        qdata[:,i] = sum(post[:, cols], 2)
        i+=1
    end
    return qdata
end


function contribute(scores, qdata, posmap, n_bases)
    kmerlen = size(posmap,1)
    for (kmer_pos, seq_pos) in enumerate(posmap)
        index = ((kmerlen - kmer_pos) * n_bases) + 1 
        scores[seq_pos, :] += qdata[index:index+n_bases-1,:]
    end 
    return
end

export form_basecall
function form_basecall(qdata, kmers, states; qscore_correction=nothing)  
    println("run form_basecall...")

    n_events = size(qdata, 1)

    kmer_path = [kmers[i] for i in states] #states are number indices for kmers

    #number of _BASES moved - e.g. 0 for AACT, AACT; 1 for TAAC, AACA; 2 for TAAC, ACGA; 
    moves = kmer_overlap(kmer_path)

    seq_len = sum(moves) + _KMER_LEN 
     
    scores = zeros(seq_len, _BASES_LEN)
   
    sequence = [join(kmer_path[1,:])]
 
    posmap = collect(1:_KMER_LEN)

    contribute(scores, qdata[1, :], posmap, _BASES_LEN)
    for (event, move) in enumerate(moves)

        if move > 0
            if move == _KMER_LEN
                posmap = collect( (posmap[end] + 1):(posmap[end] + 1 + _KMER_LEN) ) 
            else
                posmap[1:end-move] = posmap[move+1:end] #[2, 3, 4, 3, 4] if move == 2
                posmap[end-move+1:end] = collect( (posmap[end-move] + 1) : (posmap[end-move] + move) ) 
                #[2, 3, 4, 5, 6] if move == 2, [3, 4, 5, 6, 7] if next move 1
            end 
            push!(sequence, join(kmer_path[event][end-move+1:end])) #TODO attempt to access 5504*1 at [2,-2:1]
        end 
        #TODO check this 
        contribute(scores, qdata[event, :], posmap, _BASES_LEN)
    end 
    
    println("after moves:") 
    println("seq len: " * string(size(sequence, 1)) ) 

    sequence = join(sequence) 

    base_to_pos = Dict()
    for (i, b) in enumerate(_BASES)
        base_to_pos[b] = i
    end

    println("after base_to_pos dict") 

    scores += _ETA
    scoresums = sum(scores, 2)


    println("########### scoresums ###########")
    println(size(scores,1))      #5
    println(size(scores,2))      #4
    println(size(scoresums,1))   #5
    println(size(scoresums,2))   #1
    println("########### scoresums ###########")

    scores = scores ./ scoresums
    
    called_probs = [] 
    for (n, base) in enumerate(sequence)
        push!(called_probs, scores[n, base_to_pos[ string(base) ]])
    end

    if qscore_correction == "template"
        # initial scores fit to empirically observed probabilities
        #   per score using: Perror = a.10^(-bQ/10). There's a change
        #   in behaviour at Q10 so we fit two lines. (Q10 seems suspicious).
        #TODO 
        #switch_q = 10
        #a, b = 0.05524, 0.70268
        #c, d = 0.20938, 1.00776
        #switch_p = 1.0 - np.power(10.0, - 0.1 * switch_q)
        #scores = np.empty_like(called_probs)
        #for x, y, indices in zip((a,c), (b,d), (called_probs < switch_p, called_probs >= switch_p)):
        #    scores[indices] = -(10.0 / np.log(10.0)) * (y*np.log1p(-called_probs[indices]) + np.log(x))
        #end
    elseif qscore_correction in ("2d","complement")
        # same fitting as above
        if qscore_correction == "complement"
            x, y = 0.13120, 0.88952
        else
            x, y = 0.02657, 0.65590
        end 
        scores = -(10.0 / log(e, 10.0)) * (y*log(e, (-called_probs+1)) + log(e, x))
    else
        scores = -10.0 * log(e, (-called_probs+1)) / log(e, 10.0)
    end

    offset = 33
    scores = clip.( ceil(scores) .+ offset, offset, 126)

    qstring = join(scores)
    
    return sequence, qstring, kmer_path

end


end #module FormBasecall

