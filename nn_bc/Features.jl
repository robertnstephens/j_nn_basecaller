############################################
# author: Robert Stephens                  #
# email:  robert.stephens@nanoporetech.com #
############################################

module Features

using Segment

export events_to_features
function events_to_features(events; use_trim=true, trim=10, window=[-1,0,1], sloika=false)
    println("run events_to_features...") 
    
    #obtain feature vectors from events

    #param filename: path of file to read.
    #param window: list specifying event offset positions from which to
    #    derive features. A short centered window is used by default.
   
    features = Dict{String, Array{Any,1}}()

    #TODO what does sloika do??

    println("before run_segment: " * string(length(events["mean"])) * " events")

    events, split_results = run_segment(events, section="template")

    println("after run_segment: " * string(length(events["mean"])) * " events")

    deltas = initialize_sfg(events)

    #TODO make less ugly
    order = ["mean", "stdv", "length", "delta"]

    #TODO 
    debug = false
    if debug
        filename = "./data/r9_ex_events_post_mean_scale/events_out_"

        deltas  = readcsv(filename * "delta" * ".csv")[:,1]
        means   = readcsv(filename * "mean" * ".csv")[:,1]
        stdvs   = readcsv(filename * "stdv" * ".csv")[:,1]
        lengths = readcsv(filename * "length" * ".csv")[:,1]
       
    else
        deltas  = scale_array(deltas, with_mean=false)
        means   = scale_array(events["mean"])
        stdvs   = scale_array(events["stdv"])
        lengths = scale_array(events["length"])
    end


    for pos in window
        A = padded_offset_array(means, pos)
        println("length of padded_offset_array: " * string(length(A)))

        features["mean["   * string(pos) * "]"]   = padded_offset_array(means, pos)
        features["stdv["   * string(pos) * "]"]   = padded_offset_array(stdvs, pos)
        features["length[" * string(pos) * "]"]   = padded_offset_array(lengths, pos)
        features["delta["  * string(pos) * "]"]   = padded_offset_array(deltas, pos)
    end


    features_arr = features_to_arr(features, window, order)

    println("use_trim: " * string(use_trim) * ", trim: " * string(trim))
    println("pre trim num_events " * string(length(events["mean"])))
    if use_trim
        features_arr = features_arr[trim+1:end-trim, :]
        for k in keys(events)
            events[k] = events[k][trim+1:end-trim]
        end
    end
    println("after trim num_events " * string(length(events["mean"])))
    
    return events, features, features_arr

end
   

function features_to_arr(features, window, order)

    len = length( features["mean[-1]"] )

    features_arr = zeros( len, length(window)*length(order) )

    #order -1 mean, stdv, length, delta, 0 mean, stdv, etc.....

    for (i, pos) in enumerate(window)
        for (j, field) in enumerate(order)
            c = (i-1)*length(order) + j
            features_arr[:, c] = features[field * "[" * string(pos) * "]"]
        end
    end

    return features_arr
end


function initialize_sfg(events)

    #Feature vector generation from events.

    #param events standard event array.
    #param labels labels for events, only required for training

    #note:
    #The order in which the feature adding methods is called should be the
    #same for both training and basecalling.

    #TODO 
    sloika_model = false

    # Augment events - TODO: Juliaize
    if sloika_model
    #    delta = np.abs(np.ediff1d(events['mean'], to_end=0))
    #    self.events = nprf.append_fields(events, 'delta', delta)
    #    for field in ('mean', 'stdv', 'length', 'delta')
    #        scale_array(self.events[field], copy=False)
    else
    #    for field in ('mean', 'stdv', 'length',)
    #        scale_array(self.events[field], copy=False)
    
    #   delta = np.ediff1d(self.events['mean'], to_begin=0) #diff between conseq. elements in array
        delta = zeros( length(events["mean"]) )
        for i in 2:length(delta)
            delta[i] = events["mean"][i] - events["mean"][i-1]
        end
    
    #    scale_array(delta, with_mean=False, copy = False)
    #    self.events = nprf.append_fields(self.events, 'delta', delta)
    end

    return delta

end


function padded_offset_array(array, pos)
    #Offset an array and pad with zeros.
    #:param array: the array to offset.
    #:param pos: offset size, positive values correspond to shifting the
    #original array to the left (and padding the end of the output).
  
    out = zeros( length(array) )
    
    if pos == 0
        out = array
    elseif pos > 0
        out = array[pos+1:length(array)]
        z = zeros( pos ) 
        out = vcat(out, z) 

    else
        out = array[1:length(array)+pos]
        z = zeros( -pos )
        out = vcat(z, out)
    end

    return out
end


function scale_array(X; with_mean=true, with_std=true)
    #Standardize an array
    #Center to the mean and component wise scale to unit variance.
    #:param X: the data to center and scale.
    #:param with_mean: center the data before scaling.
    #:param with_std: scale the data to unit variance.
    
    if with_mean
        mean_1 = mean(X)
        X -= mean_1
        mean_2 = mean(X)

        #julia equivalent of np.allclose, isapprox is for arrays, shouldn't use pointwise though
        if abs(mean_2) > eps(Float64)
            X -= mean_2
        end 
    end 
    
    if with_std
        scale_1 = std(X)
        if scale_1 == 0.0 
            scale_1 = 1.0
        end
        X /= scale_1
        if with_mean
            mean_3 = mean(X)
            if abs(mean_3) > eps(Float64)
                X -= mean_3
            end 
        end 
    end 
    return X
end



end
