#! /usr/bin/julia

############################################
# author: Robert Stephens                  #
# email:  robert.stephens@nanoporetech.com #
############################################

for p in ("Knet","AutoGrad","ArgParse","Compat", "HDF5") #, "DataFrames") #TODO might not need last one
    Pkg.installed(p) == nothing && Pkg.add(p)
end

using Knet, ArgParse

#can only search cwd like this from 0.4 onwards
#using .ReadFast5Events

@everywhere include("ReadFast5File.jl")
using ReadFast5File

@everywhere include("./event_detection/EventDetection.jl")
using EventDetection

@everywhere include("Segment.jl")
@everywhere include("Features.jl")
@everywhere include("./utils/ParseJson.jl")
@everywhere include("./neuralnet/Network.jl")
@everywhere include("./utils/Config.jl")
@everywhere include("./utils/GenUtils.jl")
@everywhere include("./neuralnet/Decoder.jl")
@everywhere include("./basecall/FormBasecall.jl")
@everywhere include("EventsToBases.jl")
using EventsToBases

@everywhere include("Training1D.jl")
using Training1D

@everywhere include("WriteFasta.jl")
using WriteFasta


function main(args=ARGS)
    println("run") 

    s = ArgParseSettings()
    s.description="Base caller for Oxford Nanopore fast5 files using RNNs"
    s.exc_handler=ArgParse.debug_handler
    @add_arg_table s begin
        #("--atype"; default=(gpu()>=0 ? "KnetArray" : "Array"); help="array type: Array for cpu, KnetArray for gpu")
        #("--batchsize"; arg_type=Int; default=20; help="number of instances in a minibatch")
        ("--event_detect"; arg_type=Int; default=1; help="run event detection on raw data vs read events")
        ("--run_training"; arg_type=Int; default=0; help="run in training mode")
        ("--training_file"; arg_type=String; default="./tests/test.fasta"; help="desired training results")
        ("--kmer_len"; arg_type=Int; default=5; help="kmer length")
        ("--bases"; arg_type=String; default="acgt"; help="bases")
        ("--fast5"; arg_type=String; default="./tests/test.fast5"; help="fast5 file")
        ("--dir"; arg_type=String; default=nothing; help="directory of fast5 files")
        ("--fasta"; arg_type=String; default=nothing; help="output fasta")
    end
    println(s.description)
    isa(args, AbstractString) && (args=split(args))
    parsed_args = parse_args(args,s; as_symbols=true)
    println("opts=",[(k,v) for (k,v) in parsed_args]...)

    run_basecaller(parsed_args)

end

function run_basecaller(parsed_args)
    
    #ENV["LD_LIBRARY_PATH"] = "./event_detection/"
    #ENV["LD_LIBRARY_PATH"] = "/home/zen/j_nn_basecaller/nn_bc/event_detection/" 
    #println("LD_LIBRARY_PATH " * ENV["LD_LIBRARY_PATH"])

    fast5files = []

    dir = parsed_args[:dir]
    output_fasta = parsed_args[:fasta]

    if dir == nothing
        push!(fast5files, parsed_args[:fast5])
    else
        files = readdir(dir)
    
        println(files)
    
        for file in files
    
            if file[end-5:end] == ".fast5"
                push!(fast5files, dir * file)
            end
        end
    
        println(fast5files)
    end

    @parallel (+) for fast5file in fast5files

        println("processing: ", fast5file)
        
        event_detect = parsed_args[:event_detect]
    
        println("event_detect: " * string(event_detect))
        
        if event_detect != 0
            @time raw_data, meta_data, strand_id = read_raw_data(fast5file)
  
            @time events = run_event_detection(raw_data, meta_data["sampling_rate"], window_lengths=[5,10], thresholds=[2.0, 1.1], peak_height=1.2)
    
        else 
            @time events, strand_id = read_events(fast5file)
        end
        
        kmer_len = parsed_args[:kmer_len]
        bases = parsed_args[:bases]
    
        num_kmers = length(bases)^kmer_len
        println("number kmers: ", num_kmers)
       
        if parsed_args[:run_training] == 1
            training_file = parse_args[:training_file]
            @time run_training(events, training_file)
        else
            fast5_ext = ".fast5"
          
            if parsed_args[:fasta] == nothing
                fastafile = fast5file[1:end-length(fast5_ext)] * ".fasta"
            else
                fastafile = parsed_args[:fasta]
            end

            #@time sequence = events_to_bases(events)
            sequence = events_to_bases(events)

            #println(kmers * " : " * fastafile)
            @time run_write_fasta(sequence, strand_id, fastafile)
        end
    end #end loop over fast5files
end

# This allows both non-interactive (shell command) and interactive calls like:
# $ julia run_basecaller.jl --fast5 "somefile.fast5"
# julia> run_bascaller.main("--fast5 somfile.fast5")
!isinteractive() && !isdefined(Core.Main,:load_only) && main(ARGS)


