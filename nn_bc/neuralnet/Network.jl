
############################################
# author: Robert Stephens                  #
# email:  robert.stephens@nanoporetech.com #
############################################

module Network

using ParseJson


export run
function run(events, features)

    # returns posterior matrix, each row of which sums to 1.0

    #default R9
    
    #nanonet.nn.BiRNN -> LSTM, Reverse(LSTM)
    #nanonet.nn.FeedForward 
    #nanonet.nn.BiRNN -> LSTM, Reverse(LSTM)
    #nanonet.nn.FeedForward 
    #nanonet.nn.SoftMax 

    use_knet = false

    info("run network...")

    @assert size(features, 2) == 12

    ss = Serial()
    post = ss.run(ss, features)

    return post 
end

function sigmoid(z)
    return 1.0 ./ (1.0 .+ exp(-z))
end


type Serial
    layers
    in_size
    out_size
    run

    function Serial()

        #TODO random weighting for now ####

        #wsize = 96
        #Wi1 = 2*rand(4, 12, wsize) - 1.0
        #Wl1 = 2*rand(4, wsize, wsize) - 1.0

        #Wi2 = 2*rand(4, 128, wsize) - 1.0
        #Wl2 = 2*rand(4, wsize, wsize) - 1.0

        #by now iW should be (12, 384)  BiRNN layers[0]
        #by now iW should be (128, 384) BiRNN layers[2]


        #W = rand(input_size, wsize)
        #Wff = 2*rand(wsize*2, 128) - 1.0 #right weightings size for FeedForward
        #Wsm = 2*rand(128, 1025) - 1.0    #right weightings size for SoftMax

        ###################################

        json_file = "./data/r9_template.json" 
        W = parse_fields(json_file)

        #BiRNN_1
        br1_iW1 = W["br1_iW1"]
        br1_lW1 = W["br1_lW1"]
        br1_b1  = W["br1_b1"]
        br1_p1  = W["br1_p1"]

        br1_iW2 = W["br1_iW2"]
        br1_lW2 = W["br1_lW2"]
        br1_b2  = W["br1_b2"]
        br1_p2  = W["br1_p2"]

        br1_iW1 = reshape(br1_iW1, 12, 256) #right for 1st 64 cols of 12 each??????
        br1_lW1 = reshape(br1_lW1, 64, 256)
        
        #br1_p1  = reshape(br1_p1, 3, 64)
        br1_p1  = reshape(br1_p1, 64, 3)
        br1_p1 = transpose(br1_p1)

        br1_iW2 = reshape(br1_iW2, 12, 256)
        br1_lW2 = reshape(br1_lW2, 64, 256)
        
        #br1_p2  = reshape(br1_p2, 3, 64)
        br1_p2  = reshape(br1_p2, 64, 3)
        br1_p2 = transpose(br1_p2)

        #BiRNN_2
        br2_iW1 = W["br2_iW1"]
        br2_lW1 = W["br2_lW1"]
        br2_b1  = W["br2_b1"]
        br2_p1  = W["br2_p1"]

        br2_iW2 = W["br2_iW2"]
        br2_lW2 = W["br2_lW2"]
        br2_b2  = W["br2_b2"]
        br2_p2  = W["br2_p2"]


        br2_iW1 = reshape(br2_iW1, 64, 256)
        br2_lW1 = reshape(br2_lW1, 64, 256)
        
        #br2_p1  = reshape(br2_p1, 3, 64)
        br2_p1  = reshape(br2_p1, 64, 3)
        br2_p1 = transpose(br2_p1)


        br2_iW2 = reshape(br2_iW2, 64, 256)
        br2_lW2 = reshape(br2_lW2, 64, 256)
        
        #br2_p2  = reshape(br2_p2, 3, 64)
        br2_p2  = reshape(br2_p2, 64, 3)
        br2_p2 = transpose(br2_p2)


        #FF_1
        ff1_W = W["ff1_W"]
        ff1_b = W["ff1_b"]

        #FF_2
        ff2_W = W["ff2_W"]
        ff2_b = W["ff2_b"]
        
        #SM
        sm_W = W["sm_W"]
        sm_b = W["sm_b"]


        ff1_W = reshape(ff1_W, 128, 64)
        ff2_W = reshape(ff2_W, 128, 64)
        
        sm_W = reshape(sm_W, 64, 1025)


        bi_rnn1 = BiRNN(br1_iW1, br1_lW1, br1_iW2, br1_lW2, br1_b1, br1_p1, br1_b2, br1_p2)
        bi_rnn2 = BiRNN(br2_iW1, br2_lW1, br2_iW2, br2_lW2, br2_b1, br2_p1, br2_b2, br2_p2)
        
        ff1     = FeedForward(ff1_W, b=ff1_b)
        ff2     = FeedForward(ff2_W, b=ff2_b)
        sm     = SoftMax(sm_W, b=sm_b) # sm_W 64*1025

        layers = [bi_rnn1, ff1, bi_rnn2, ff2, sm]
        
        info("###############################")
        for i in range(1, length(layers))
            info(string(i) * " : " * string(layers[i].in_size) * " : " * string(layers[i].out_size) )
        end
        info("###############################")


        prev_out_size = layers[1].out_size
        for i in range(2, length(layers)-1)
            @assert prev_out_size == layers[i].in_size #, "Incompatible shapes: {} -> {} in layers {}.\n".format(prev_out_size, layers[i].in_size, i)
            prev_out_size = layers[i].out_size
        end

        in_size = layers[1].in_size     #12 by R9 default
        out_size = layers[end].out_size #1025 by R9 default, so 4^5+1
        
        run = run_serial

        new(layers, in_size, out_size, run)

    end

end


function run_serial(self, inMat; ctx=nothing, queueList=nothing)

    info("run_serial...")

    if queueList == nothing
        @assert self.in_size == size(inMat, 2) #should be 12
        tmp = inMat
      
        for layer in self.layers
            tmp = layer.run(layer, tmp)
        end 
        return tmp
    else
        for mat in inMat
            @assert self.in_size == size(mat, 2)
        end 

        tmp = inMat
        for layer in self.layers
            tmp = layer.run(layer, tmp, ctx, queueList)
        end
 
        return tmp
    end

end




#TODO
type Reverse
    #Runs a recurrent layer in reverse time (backwards).
    layer 
    in_size
    out_size
    run

    function Reverse(layer_in)
        layer = layer_in
        in_size = layer_in.in_size
        out_size = layer_in.out_size

        run = run_reverse
        new(layer, in_size, out_size, run)
    end
end
   

function run_reverse(self, inMat; ctx=nothing, queueList=nothing)
    info("run_reverse...") 

    if queueList == nothing
        @assert self.in_size == size(inMat, 2)
        return self.layer.run(self.layer, inMat[end:-1:1, 1:end])[end:-1:1, 1:end] #checked
    else
        inMatList = []
        for mat in inMat
            @assert self.in_size == size(mat,2)
            #TODO 
            #inMatList.append(mat[::-1])
        end 
        postList= self.layer.run(self.layer, inMatList, ctx, queueList)
        postListTmp = []
        for post in postList
            #TODO 
            #postListTmp.append(post[::-1])
        end 
        return postListTmp
    end

end


type Parallel
    layers
    in_size
    out_size
    run

    function Parallel(layers_in)

        layers = layers
        in_size  = layers[1].in_size
        out_size = layers[1].out_size   
     
        for i in range(2, length(layers))
            @assert in_size == layers[i].in_size
            out_size += layers[i].out_size
        end
    
        run = run_parallel
    end
end


function run_parallel(self, inMat, layers; ctx=nothing, queueList=nothing)
     
    if queueList == nothing
        return cat(2, layers[1].run(inMat), layers[2].run(inMat)) 
    else
        error("ERROR run_parallel") 
    end
end


type BiRNN
    layers
    in_size 
    out_size 
    run
    function BiRNN(iW1, lW1, iW2, lW2, b1, p1, b2, p2)
      

        println("size iW1:", size(iW1))
        println("size iW2:", size(iW2))


        lstm1 = LSTM(iW1, lW1, b=b1, p=p1) 
        lstm2 = LSTM(iW2, lW2, b=b2, p=p2) 
        
        rev  = Reverse(lstm2) 
      
        layers = []

        push!(layers, lstm1)
        push!(layers, rev)

        in_size = layers[1].in_size

        out_size = 0
        for layer in layers
            out_size += layer.out_size
        end

        run = run_BiRNN
    
        new(layers, in_size, out_size, run) 
    end

end


function run_BiRNN(self, inMat; ctx=nothing, queueList=nothing) 
    info("run_BiRNN...")

    outMat = cat(2, self.layers[1].run(self.layers[1],inMat), self.layers[2].run(self.layers[2],inMat)) #cat(2, equiv of np.hstack

    return outMat

end


type FeedForward
    #Basic feedforward layer
    # 
    #          out = f(inMat W + b)
    # 
    #:param W: Weight matrix of dimension (|input|, size)
    #:param b: Bias vector of length size. Optional with default of no bias.
    #:param fun: The activation function.  Must accept a numpy array as input.
    W
    b #np.zeros(W.shape[1], dtype=dtype) if b is None else b
    f #fun == tanh
    in_size
    out_size
    run

    function FeedForward(W; b=nothing, fun=tanh)
        
        @assert b == nothing || length(b) == size(W, 2)
           
        if b == nothing 
            b = zeros(size(W, 2)) 
        else   
            b = b 
        end       
  
        f = fun
        in_size  = size(W, 1)
        out_size = size(W, 2)

        run = run_feedforward
    
        new(W, b, f, in_size, out_size, run) 
    end
end

function run_feedforward(self, inMat; ctx=nothing, queueList=nothing) 
    info("run_feedforward...") 

    if queueList != nothing
        error("ERROR: feedforward: opencl not yet supported") 
    else
        @assert self.in_size == size(inMat, 2)
       

        println("ff10 ", typeof(inMat))
        println("ff20 ", typeof(self.W))
        println("ff30 ", typeof(self.b))


        println("ff1 ", size(inMat))
        println("ff2 ", size(self.W))
        
        dotprod = inMat*self.W
        for i in 1:size(dotprod, 1)
            dotprod[i,:] += self.b
        end 

        println("ff3 ", size(dotprod))

        println("ff4 ", typeof(dotprod))

        return self.f(dotprod) 
    end
end


type SoftMax
    #SoftMax layer
    #tmp = exp(inmat W + b)
    #out = row_normalise(tmp)

    #:param W: Weight matrix of dimension (|input|, size)
    #:param b: Bias vector of length size.  Optional with default of no bias.
    
    W 
    b 
    in_size
    out_size 
    run

    function SoftMax(W; b=nothing)

        @assert b == nothing || length(b) == size(W, 2)
    
        if b == nothing 
            b = zeros(size(W, 2))
        else
            b = b 
        end
     
        W = W
 
        in_size = size(W, 1)
        out_size = size(W, 2)

        run = run_softmax
      
        new(W, b, in_size, out_size, run)
    end
end


function run_softmax(self, inMat, ctx=nothing, queueList=nothing)
    info("run_softmax...") 

    if queueList != nothing
        error("ERROR: softmax: opencl currently not supported")
    else
        @assert self.in_size == size(inMat, 2)
        tmp =  inMat*self.W 
        for i in 1:size(tmp,1)
            tmp[i,:] += self.b 
        end

        m, aindx = findmax(tmp,2) #TODO don't appear to need reshape(-1,1) here, in Julia

        for j in 1:size(tmp,2)
            tmp[:,j] -= m
        end
        tmp = exp(tmp)
        
        x = sum(tmp, 2)
 
        tmp ./= x
        
        return tmp
    end
end



type LSTM
    iW
    lW 
    b #nothing by defualt
    p #nothing by default
    in_size  
    out_size 
    size3 
    step
    run
    function LSTM(iW, lW; b=nothing, p=nothing)
        #Long short-term memory layer with peepholes. Implementation is to be consistent with
        #Currennt and may differ from other descriptions of LSTM networks (e.g.
        #http://colah.github.io/posts/2015-08-Understanding-LSTMs/).
    
        #    Step:
        #        v = [ input_new, output_old ]
        #        Pforget = sigmoid( v W2 + b2 + state * p1)
        #        Pupdate = sigmoid( v W1 + b1 + state * p0)
        #        Update  = tanh( v W0 + b0 )
        #        state_new = state_old * Pforget + Update * Pupdate
        #        Poutput = sigmoid( v W3 + b3 + state * p2)
        #        output_new = tanh(state) * Poutput
        #
        #:param iW: weights for cells taking input from preceeding layer.
        #    Size (4, -1, size)
        #:param lW: Weights for connections within layer
        #    Size (4, size, size )
        #:param b: Bias weights for cells taking input from preceeding layer.
        #    Size (4, size)
        #:param p: Weights for peep-holes
        #    Size (3, size)
      
        reshape_needed = false

        if reshape_needed #iW 4 * 3 * size
            @assert ndims(iW)   == 3
            @assert size(iW, 1) == 4 
        
            size3 = size(iW, 3)
        
            @assert ndims(lW) == 3
            @assert size(lW) == (4, size3, size3)
            
            if b == nothing
                b = zeros(4, size3)
            end 
            @assert size(b) == (4, size3) 
            
            if p == nothing
                p = zeros(3, size3) 
            end
            @assert size(p) == (3, size3)
            
            iW = transpose( reshape(iW, (4*size3, size(iW,2))) )  
            lW = transpose( reshape(lW, (4*size3, size3)) )  
            b = reshape(b, length(b) ) 
            p = reshape(p, length(p)) 
        else 
            size3 = Int( size(iW, 2) / 4)
        end 
        out_size = size3

        in_size = size(iW, 1)  

        step = step_lstm
        run = run_lstm
        
        new(iW, lW, b, p, in_size, out_size, size3, step, run)
    end
end



function step_lstm(self, in_vec, in_state)
    
    vW = transpose(in_vec)*self.iW #12*1 12*384

    out_prev, prev_state = in_state
    outW = transpose(out_prev)*self.lW
   
    sumW = vW + outW  + transpose(self.b)
    
    sumW = transpose( reshape(sumW, (self.size3,4)) )


    #  Forget gate activation
    state = prev_state .* sigmoid(sumW[3,:] + prev_state .* self.p[2,:] )
    #  Update state with input
    state += tanh(sumW[1,:]) .* sigmoid(sumW[2,:] + prev_state .* self.p[1,:])
    #  Output gate activation
    out = tanh(state) .* sigmoid(sumW[4,:]  + state .* self.p[3,:])
    return out, state
end


function run_lstm(self, inMat; ctx=nothing, queueList=nothing)
    info("run_lstm...") 

    if queueList != nothing
        error("ERROR: LSTM: opencl currently not supported") 
    else
        @assert self.in_size == size(inMat, 2)

        #out = np.zeros((inMat.shape[0], self.out_size), dtype=dtype)
        out = zeros( (size(inMat,1), self.out_size) )
         
        out_prev = zeros(self.out_size)
        
        state = zeros(self.out_size)

        for i in 1:size(inMat, 1)
            v = inMat[i,1:size(inMat,2)]  #TODO less ugly way of iterating through rows?
            out_prev, state = self.step(self, v, (out_prev, state))
            out[i,:] = out_prev #TODO guess
        end

        return out
    end
end


end #Nework



