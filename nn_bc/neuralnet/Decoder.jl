############################################
# author: Robert Stephens                  #
# email:  robert.stephens@nanoporetech.com #
############################################

module Decoder

using Config

export fast_estimate_transitions
function fast_estimate_transitions(post; trans=nothing)
    #Naive estimate of transition behaviour from posteriors
    #:param post: posterior probabilities of kmers by event.
    #:param trans: prior belief of transition behaviour (None = use global estimate)

    println("fast_estimate_transitions...")

    @assert trans == nothing || size(trans, 2) == 3
    res = zeros( (size(post, 1), 3) )
    fill!(res, _ETA)

    #func = nanonetdecode.estimate_transitions
    func = estimate_transitions_py
    #func = estimate_transitions_cpp
    #TODO WRONG it calls C++ function ./nanonet/decoding.cpp +151
    
    #func.restype = None
    #func.argtypes = [ndpointer(dtype=np.float32, flags='CONTIGUOUS'),
    #                 ndpointer(dtype=np.float32, flags='CONTIGUOUS'),
    #                 c_size_t, c_size_t, c_size_t]
    
    #func(post, res, post.shape[0], n_bases, post.shape[1])
    #func(post, res, size(post, 1), n_bases, size(post, 2) ) #C++ version
    res = func(post, res) 

    if trans == nothing
        trans = sum(res, 1)
        trans /= sum(trans)
    end

    res .*= trans
    res = res ./ sum(res, 2)

    return res

end


export fast_run_decoder
function fast_run_decoder(post, trans; use_log=true, slip=0.0)
    #decode_profile()
    #Viterbi-style decoding with per-event transition weights (profile)
    #:param post: posterior probabilities of kmers by event.
    #:param trans: A generator (e.g. a :class:`ndarray`) to produce per-transition log-scaled weights. None == no transition weights.
    #:param log: Posterior probabilities are in log-space.
  
    println("fast_run_decoder...")

    nstate = size(post, 2)
    println("nstate")
    println(nstate)

    lpost = copy(post) 
   
    if use_log == true
        println("use_log == true") 
        lpost += _ETA 
        lpost = log(e, lpost) 
    end

    if trans == nothing
        #trans = itertools.repeat(np.zeros(3))
    else
        trans = log(e, (_ETA + trans))  
       
        trans = copy(trans) #trans is [[stay1, step1, skip1], [stay2, step2, skip2], .... num events]
        trans[:,2] -= _STEP_FACTOR # -= ln(4)
        trans[:,3] -= _SKIP_FACTOR # -= ln(16)
    end

    log_slip = log(e, (_ETA + slip))

    pscore = lpost[1,:]

    for ev in 2:size(post,1)
        # Forward Viterbi iteration
        
        ev_trans = trans[ev-1,:] 

        ########
        # Stay #
        ########

        score = pscore + ev_trans[1]
        iscore = collect(1:nstate)

        ########
        # Slip #
        ########




        ########
        # Step #
        ########




        ########
        # Skip #
        ########



    end

end


export run_decoder
function run_decoder(post, trans; use_log=true, slip=0.0)
    #decode_profile()
    #Viterbi-style decoding with per-event transition weights (profile)
    #:param post: posterior probabilities of kmers by event.
    #:param trans: A generator (e.g. a :class:`ndarray`) to produce per-transition log-scaled weights. None == no transition weights.
    #:param log: Posterior probabilities are in log-space.
  
    println("run_decoder...")

    nstate = size(post, 2)
    println("nstate")
    println(nstate)

    lpost = copy(post) 
   
    if use_log == true
        println("use_log == true") 
        lpost += _ETA 
        lpost = log(e, lpost) 
    end

    if trans == nothing
        #trans = itertools.repeat(np.zeros(3))
    else
        trans = log(e, (_ETA + trans))  
       
        trans = copy(trans) #trans is [[stay1, step1, skip1], [stay2, step2, skip2], .... num events]
        trans[:,2] -= _STEP_FACTOR # -= ln(4)
        trans[:,3] -= _SKIP_FACTOR # -= ln(16)
    end

    log_slip = log(e, (_ETA + slip))

    pscore = lpost[1,:]
   
    optim = true #TODO

    for ev in 2:size(post,1)
        # Forward Viterbi iteration
        
        ev_trans = trans[ev-1,:] 

        ########
        # Stay #
        ########

        score = pscore + ev_trans[1]
        iscore = collect(1:nstate)

        ########
        # Slip #
        ########

        scoreNew, iscoreNew = findmax(pscore)
        scoreNew += log_slip 
      
        #TODO: better way
        for i in 1:length(score)
            if score[i] > scoreNew
                iscore[i] = iscore[i]      
            else
                iscore[i] = iscoreNew
            end 
        end

        @assert maximum(iscore) <= 1024
        @assert minimum(iscore) >= 1

        score = max(score, scoreNew) #element-wise max

        ########
        # Step #
        ########


        pscore = transpose( reshape(pscore, Int(length(pscore)/_NSTEP), _NSTEP) )
        nrem = size(pscore, 2) 
        
        scoreNew, iscoreNew = findmax(pscore, 1) 
        iscoreNew = ind2sub(size(pscore), vec(iscoreNew))[1]
        scoreNew = transpose(scoreNew)     
       

        if !optim
            scoreNew  = repeat( scoreNew + ev_trans[2], outer=[1,_NSTEP])                    #5970 frames
            iscoreNew = repeat( (nrem .* (iscoreNew-1)) + collect(1:nrem), outer=[1,_NSTEP]) #2138 frames
        else
            temp_scoreNew = zeros(length(scoreNew), _NSTEP)
            temp_iscoreNew = Array{Int64}(length(scoreNew), _NSTEP)
    
            for col in 1:_NSTEP
                temp_scoreNew[:,col] = scoreNew + ev_trans[2]
                temp_iscoreNew[:,col] = (nrem .* (iscoreNew-1)) + collect(1:nrem)
            end
            
            scoreNew = temp_scoreNew
            iscoreNew = temp_iscoreNew
        end

        scoreNew = reshape(transpose(scoreNew), length(scoreNew), 1)
        iscoreNew = reshape(transpose(iscoreNew), length(iscoreNew), 1)

        #TODO: better way
        for i in 1:length(score)
            if score[i] > scoreNew[i]
                iscore[i] = iscore[i]      
            else
                iscore[i] = iscoreNew[i]      
            end 
        end

        @assert maximum(iscore) <= 1024
        @assert minimum(iscore) >= 1

        score = max(score, scoreNew) #element-wise max

        ########
        # Skip #
        ######## 

        pscore = transpose( reshape(pscore, Int(length(pscore)/_NSKIP), _NSKIP) )
        nrem = size(pscore, 2) 
       
        scoreNew, iscoreNew = findmax(pscore, 1) 
        iscoreNew = ind2sub(size(pscore), vec(iscoreNew))[1]
        scoreNew = transpose(scoreNew)

        if !optim
            scoreNew  = repeat( scoreNew + ev_trans[3], outer=[1,_NSKIP])                       #5458 frames
            iscoreNew = repeat( (nrem .* (iscoreNew - 1) ) + collect(1:nrem), outer=[1,_NSKIP]) #1824 frames
        else
            temp_scoreNew = zeros(length(scoreNew), _NSKIP)
            temp_iscoreNew = Array{Int64}(length(scoreNew), _NSKIP)
    
            for col in 1:_NSKIP
                temp_scoreNew[:,col] = scoreNew + ev_trans[3]
                temp_iscoreNew[:,col] = (nrem .* (iscoreNew-1)) + collect(1:nrem)
            end
            
            scoreNew = temp_scoreNew
            iscoreNew = temp_iscoreNew
        end

        scoreNew = reshape(transpose(scoreNew), length(scoreNew), 1)
        iscoreNew = reshape(transpose(iscoreNew), length(iscoreNew), 1)


        for i in 1:length(score)
            if score[i] > scoreNew[i]
                iscore[i] = iscore[i]      
            else
                iscore[i] = iscoreNew[i]      
            end 
        end

        @assert maximum(iscore) <= 1024
        @assert minimum(iscore) >= 1

        score = max(score, scoreNew) 
      
        #########
        # Store #
        ######### 
        
        lpost[ev-1,:] = iscore
      
        pscore = score + lpost[ev,:]
    end

    println("before viterbi backtrace")
    println( "max lpost: " * string(maximum(lpost)) * ", min lpost: " * string(minimum(lpost)) )

    state_seq = zeros( Int64, size(post, 1) ) 
    
    state_seq[end] = findmax(pscore)[2] 

    for ev in size(post,1):-1:2
        state_seq[ev-1] = Int(floor(lpost[ev-1, state_seq[ev]]))
    end

    return maximum(pscore), state_seq

end


export estimate_transitions_py #export for unit testing
function estimate_transitions_py(post, res)
    #Naive estimate of transition behaviour from posteriors
    #:param post: posterior probabilities of kmers by event.
    #:param trans: prior belief of transition behaviour (None = use global estimate)
    ################################################################################

    println("estimate_transitions_py...")

    @assert res == nothing || size(res, 2) == 3 #'Incorrect number of transitions'

    if res == nothing
        res = zeros( (size(post,1), 3) ) #res num events * 3
        fill!(res, _ETA)
    end

    for ev in 2:size(post, 1) #rs_mod iterate over all events
        stay = sum(post[ev-1,:] .* post[ev,:]) #1*num_states * 1*num_states -> 1*num_states, higher values for same post rows

        #num_states/_NSTEP rows * _NSTEP columns
        p = transpose( reshape(post[ev,:], _NSTEP, Int(length(post[ev,:])/_NSTEP) ) )

        #AAA AAC AAG AAT ACA ACC ACG ACT, etc ->
        
        #AAA AAC AAG AAT -> sum AA*
        #ACA ACC ACG ACT -> sum AC*
        #AGA AGC AGG AGT -> sum AG*
        #ATA ATC ATG ATT -> sum AT*

        #post[ev-1]      .* ss
        #AAA AAC AAG etc *  sum AA*
        #AAA AAC AAG etc *  sum AC*

        ss = []
        s = sum(p,2) #sum rows
        for i in 1:_NSTEP 
            ss = vcat(ss, s) #ss is num_states*1  
        end
        step = sum(post[ev-1,:] .* ss) / _NSTEP #1*num_states*num_states*1 -> sum(num_states * num_states matrix)
 
        p = transpose( reshape( post[ev, :], _NSKIP, Int(length(post[ev,:])/_NSKIP)) ) 

        ss = []
        s = sum(p,2)
        for i in 1:_NSKIP
            ss = vcat(ss, s) 
        end
        skip = sum(post[ev-1,:] .* ss) / _NSKIP
        
        res[ev-1,:] = [stay, step, skip]
    end

    return res
end

export estimate_transitions_cpp #export for unit testing
function estimate_transitions_cpp(post, trans)
    num_events = size(post, 1)
    num_kmers = _BASES_LEN^_KMER_LEN
    
    println(num_events)
    println(num_kmers)

    _BASES_LEN_sq = _BASES_LEN * _BASES_LEN;

    #TODO because Julia iterates first by column - should change usage of post throughout entire program for efficiency/less code
    post = reshape(transpose(post), size(post,1), size(post,2) )

    for ev in 1:num_events-1
        stay_sum, step_sum, skip_sum = 0.0, 0.0, 0.0

        idx1 = ev * num_kmers    #max: num_events*num_kmers 
        idx0 = idx1 - num_kmers  #max: num_events*num_kmers - num_kmers
        
        for i in 0:Int(num_kmers/_BASES_LEN_sq)-1
            sum16 = 0.0 
            for j in 0:_BASES_LEN-1
                sum4 = 0.0
                for k in 0:_BASES_LEN-1
                    kmer = i * _BASES_LEN_sq + j * _BASES_LEN + k
                    p = post[idx1 + kmer + 1]
                    stay_sum += post[idx0 + kmer + 1] * p
                    sum4 += p 
                end

                for step_from in (_BASES_LEN*i + j):Int(num_kmers/_BASES_LEN):num_kmers-1
                    step_sum += sum4 * post[idx0 + step_from + 1]
                end
                sum16 += sum4 
            end
            
            for skip_from in i:Int(num_kmers/_BASES_LEN_sq):num_kmers-1
                skip_sum += sum16 * post[idx0 + skip_from + 1] 
            end
        
        end 
        
        step_sum *= 0.25
        skip_sum *= 0.0625
       
        trans[ev, 1] = stay_sum
        trans[ev, 2] = step_sum
        trans[ev, 3] = skip_sum
    end
    
    return trans
end


end #module Decoder





