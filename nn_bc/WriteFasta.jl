module WriteFasta

export run_write_fasta
function run_write_fasta(sequence, strand_id, fileout)

    println("Writing " * fileout * " ...")

    line_break = 80

    open(fileout, "w") do f
        write(f, ">" * strand_id * "\n")

        for i in 1:line_break:length(sequence)-line_break+1
            write(f, sequence[i:i+line_break-1])
            write(f, "\n")
        end

        last_chars = length(sequence) % line_break
        if last_chars != 0
            write(f, sequence[end-last_chars+1:end])
            write(f, "\n")
        end
    end
end

end
