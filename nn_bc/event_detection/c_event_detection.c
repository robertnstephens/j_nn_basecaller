#include <stddef.h>
#include <math.h>
#include <stdlib.h>
#include <stdio.h>
#include <assert.h>
#include <stdint.h>
#include <float.h>
#include <math.h>

#if defined(_MSC_VER)
#   define false   0
#   define true    1
#   define bool int
#   define _Bool int
#   define fmax max
#   define fmin min
#else
#   include <stdbool.h>
#endif

typedef struct {
    int DEF_PEAK_POS;
    double DEF_PEAK_VAL;
    //double * signal;
    size_t signal_length;
    double threshold;
    size_t window_length;
    size_t masked_to;
    int peak_pos;
    double peak_value;
    _Bool valid_peak;
} Detector;
typedef Detector * DetectorPtr;



void compute_sum_sumsq(double* data, double* sum, double* sumsq, int d_length) {
    
    size_t i;
  
    // Basic contracts
    assert(NULL!=data);
    assert(NULL!=sum);
    assert(NULL!=sumsq);
    assert(d_length>0);
  
    sum[0] = data[0];
    sumsq[0] = data[0]*data[0];
    for (i = 1; i < d_length; ++i) {
        sum[i] = sum[i - 1] + data[i];
        sumsq[i] = sumsq[i - 1] + data[i]*data[i];
    }
}


void compute_tstat(double* sum, double* sumsq, double* tstat, int d_length, int w_length, bool pooled) {

    int i;
    const double eta = 1e-100;
  
    // Simple contracts
    assert(NULL!=sum);
    assert(NULL!=sumsq);
    assert(NULL!=tstat);
  
    // Quick return:
    //   t-test not defined for number of points less than 2
    //   need at least as many points as twice the window length
    if (d_length < 2*w_length || w_length < 2) {
        for(i = 0; i < d_length; ++i){
            tstat[i] = 0.0;
        }
        return;
    }
  
    // fudge boundaries
    for (i = 0; i < w_length; ++i) {
        tstat[i] = 0;
        tstat[d_length - i - 1] = 0;
    }
  
    double sum1, sum2, sumsq1, sumsq2, mean1, mean2, var1, var2;
  
    for (i = w_length; i <= d_length - w_length; ++i) {
        sum1 = sum[i - 1];
        sumsq1 = sumsq[i - 1];
        if (i > w_length) {
            sum1 -= sum[i - w_length - 1];
            sumsq1 -= sumsq[i - w_length - 1];
        }
        sum2 = sum[i + w_length - 1] - sum[i - 1];
        sumsq2 = sumsq[i + w_length - 1] - sumsq[i - 1];
        mean1 = sum1 / w_length;
        mean2 = sum2 / w_length;
        var1 = sumsq1 / w_length - mean1*mean1;
        var2 = sumsq2 / w_length - mean2*mean2;
        if(pooled){
            var1 = ( var1 + var2 ) / 2.0;
            var2 = var1;
        }
        // Prevent problem due to very small variances
        var1 = fmax(var1, eta);
        var2 = fmax(var2, eta);
  
        //t-stat
        //  Formula is a simplified version of Student's t-statistic for the
        //  special case where there are two samples of equal size with
        //  differing variance
        const double delta = mean2 - mean1;
        const double totvar = var1 / w_length + var2 / w_length;
        tstat[i] = fabs(delta / sqrt(totvar));
   
        //printf("%lf\n", tstat[i]);
    }
}

void short_long_peak_detector(double* short_signal, double* long_signal, DetectorPtr short_detector, DetectorPtr long_detector, const double peak_height, size_t* peaks){
    size_t i, k;
    size_t peak_count = 0;
    DetectorPtr detector;
    DetectorPtr detectors[2] = {short_detector, long_detector};
    double current_value;

    assert(short_detector->signal_length == long_detector->signal_length);
    assert(NULL!=peaks);
    
    /*
    printf("signal: \n%lf\n %lf\n, %lf\n", short_signal[100], long_signal[100], peak_height);

    printf("short detector: \n%d\n %lf\n %d\n %lf\n %d\n %d\n %d\n %lf\n", short_detector->DEF_PEAK_POS, short_detector->DEF_PEAK_VAL, short_detector->signal_length, short_detector->threshold, short_detector->window_length, short_detector->masked_to, short_detector->peak_pos, short_detector->peak_value);
  
    printf("long detector: \n%d\n %lf\n %d\n %lf\n %d\n %d\n %d\n %lf\n", long_detector->DEF_PEAK_POS, long_detector->DEF_PEAK_VAL, long_detector->signal_length, long_detector->threshold, long_detector->window_length, long_detector->masked_to, long_detector->peak_pos, long_detector->peak_value);
    */

    /* 
    int DEF_PEAK_POS;
    double DEF_PEAK_VAL;
    //double * signal;
    size_t signal_length;
    double threshold;
    size_t window_length;
    size_t masked_to;
    int peak_pos;
    double peak_value;
    _Bool valid_peak;
    */


    for(i=0; i<short_detector->signal_length; i++){
        for(k=0; k<2; k++){
            detector = detectors[k];
            //Carry on if we've been masked out
            if (detector->masked_to >= i){
                continue;
            }
            
            if(k == 0) {
                current_value = short_signal[i];
            } 
            else {
                current_value = long_signal[i];
            }

            if (detector->peak_pos == detector->DEF_PEAK_POS){
                //CASE 1: We've not yet recorded a maximum
                if (current_value < detector->peak_value){
                    //Either record a deeper minimum...
                    detector->peak_value = current_value;
                }
                else if (current_value - detector->peak_value > peak_height){
                    // ...or we've seen a qualifying maximum
                    detector->peak_value = current_value;
                    detector->peak_pos = i;
                    //otherwise, wait to rise high enough to be considered a peak
                }
            }
            else {
                //CASE 2: In an existing peak, waiting to see if it is good
                if (current_value > detector->peak_value){
                    //Update the peak
                    detector->peak_value = current_value;
                    detector->peak_pos = i;
                }

                //Dominate other tstat signals if we're going to fire at some point
                if (detector == short_detector){
                    if (detector->peak_value > detector->threshold){
                        long_detector->masked_to = detector->peak_pos + detector->window_length;
                        long_detector->peak_pos = long_detector->DEF_PEAK_POS;
                        long_detector->peak_value = long_detector->DEF_PEAK_VAL;
                        long_detector->valid_peak = false;
                    }
                }

                //Have we convinced ourselves we've seen a peak
                if (detector->peak_value - current_value > peak_height && detector->peak_value > detector->threshold){
                    detector->valid_peak = true;
                }

                //Finally, check the distance if this is a good peak
                if (detector->valid_peak && (i - detector->peak_pos) > detector->window_length / 2){
                    //Emit the boundary and reset
                    peaks[peak_count] = detector->peak_pos;
                    peak_count++;
                    detector->peak_pos = detector->DEF_PEAK_POS;
                    detector->peak_value = current_value;
                    detector->valid_peak = false;
                }
            }
        }
    }

}


