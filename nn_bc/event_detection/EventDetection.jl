module EventDetection

export run_event_detection

immutable Detector
    DEF_PEAK_POS::Cint
    DEF_PEAK_VAL::Cdouble
    #_signal::POINTER(c_double))
    signal_length::Csize_t
    threshold::Cdouble
    window_length::Csize_t
    masked_to::Csize_t
    peak_pos::Cint
    peak_value::Cdouble
    valid_peak::Bool
end

type Event
    mean::Float64
    stdv::Float64
    start::Float64
    length::Float64
end

function compute_sum_sumsq(raw_data)

    d_length = length(raw_data)
    sums   = Array(Float64, d_length)
    sumsqs = Array(Float64, d_length)

    #void compute_sum_sumsq(const double* data, double* sum, double* sumsq, size_t d_length) {
    ccall( (:compute_sum_sumsq, "libevent_detection"), Void, (Ptr{Float64}, Ptr{Float64}, Ptr{Float64}, Int32), 
                                                              raw_data, sums, sumsqs, d_length)

    return sums, sumsqs
end

function compute_tstat(sums, sumsqs, w_len; pooled_var=false)

    d_length = length(sums)
    tstat = zeros(d_length)  
    ##f(sums, sumsqs, tstat, length, w_len, pooled_var)
    #void compute_tstat(double* sum, double* sumsq, double* tstat, size_t d_length, size_t w_length, bool pooled) 
    ccall( (:compute_tstat, "libevent_detection"), Void, (Ptr{Float64}, Ptr{Float64}, Ptr{Float64}, Int32, Int32, Bool), 
                                                          sums, sumsqs, tstat, d_length, w_len, pooled_var)

    return tstat

end


function short_long_peak_detector(t_stats, thresholds, window_lengths, peak_height)
    """Peak detector for two signals. Signal derived from shorter window
    length takes precedence.

    Transcribed from find_events_single_scale in wavelet_util.cpp.
    Equivalent to the MinKNOW implementation as of 25/06/2014.

    :param t_stats: Length 2 list of t-statistic signals
    :param thresholds: Length 2 list of thresholds on t-statistics
    :param window_lengths: Length 2 list of window lengths across
        raw data from which `t_stats` are derived
    :peak_height: Absolute height a peak in signal must rise below
        previous and following minima to be considered relevant

    This should be transcribed back to C for speed.

    note:: The peak_height parameter here is rather odd.
    """

    # Create objects analogous to original code, and sort according to length
    #detectors = [
    #    Detector(x[0], x[1], x[2]) for x in zip (t_stats, thresholds, window_lengths)
    #]
    #if detectors[0].window_length > detectors[1].window_length:
    #    detectors = detectors[::-1]


    detectors = []
    #def __init__(self, signal, threshold, window_length):

    for i in 1:size(t_stats,1)
        detector = Ref(Detector(-1,                  #DEF_PEAK_POS
                                1e100,               #DEF_PEAK_VAL = 1e100,
                                length(t_stats[i]),  #signal_length
                                thresholds[i],
                                window_lengths[i],
                                0,            #masked_to
                                -1,           #DEF_PEAK_POS,
                                1e100,        #DEF_PEAK_VAL,
                                false))       #valid_peak
        push!(detectors, detector)
    end 

    peaks = zeros(Csize_t, length(t_stats[1]) )
    #peaks = Array(Csize_t, length(t_stats[1]))

    ccall((:short_long_peak_detector, "libevent_detection"), Void, (Ptr{Float64}, Ptr{Float64}, Ref{Detector}, Ref{Detector}, Float64, Ptr{Csize_t}), 
                                                                    t_stats[1], t_stats[2], detectors[1], detectors[2], peak_height, peaks)

    
    peaks = [p for p in filter(p -> p > 0, peaks)] 
    return peaks

    #f = nanonetfilters.short_long_peak_detector
    #f.restype = None
    #f.argtypes = [POINTER(Detector), POINTER(Detector), c_double, ndpointer(dtype=c_size_t, flags='CONTIGUOUS')]
    #peaks = np.zeros(len(detectors[0].signal), dtype=c_size_t)
    #f(detectors[0], detectors[1], peak_height, peaks)

    #peaks = peaks[np.nonzero(peaks)]
    #return peaks.astype(int)

end



function _construct_events(sums, sumsqs, edges, sample_rate)
    """Creates event data from sums and sumsqs of raw data
    and a list of boundaries

    :param sums: Cumulative sum of raw data points
    :param sumsqs: Cumulative sum of squares of raw data points

    :returns: a 1D :class:`numpy.array` containing event data


    .. note::
       edges is internally trimmed such that all indices are in the
       open interval (0,len(sums)). Events are then constructed as

       [0, edges[0]), [edges[0], edges[1]), ..., [edges[n], len(sums))

    """
    @assert length(sums) == length(sumsqs) #, "sums and sumsqs must be the same length, got {} and  {}".format(len(sums), len(sumsqs))
    #@assert np.all(edges >= 0), "all edge indices must be positive"
    #@assert np.all( map( lambda x: isinstance( x, int ), edges ) )
    
    # We could check maximal values here too, but we're going to get rid of them below.
    # We put some faith in the caller to have read the DocString above.

    #edges = [i for i in edges if i>0 and i<len(sums)]
    #edges.append(len(sums))

    edges = [i for i in filter(i -> i>0 && i<length(sums), edges)]
    push!(edges, length(sums))

    num_events = length(edges)
    #if sample_rate is not None:
    #    events = np.empty(num_events, dtype=[('start', float), ('length', float), ('mean', float), ('stdv', float)])
    #else:
    #    events = np.empty(num_events, dtype=[('start', int), ('length', int), ('mean', float), ('stdv', float)])

    events = Dict{String, Array{Any,1}}()
  
    events["mean"], events["stdv"], events["start"], events["length"] = [],[],[],[]

    #events = Array(Event, num_events)

    s, sm, smsq = 0, 0, 0 #s is start index
    for (i, e) in enumerate(edges)
        #events['start'][i] = s
     
        #event = Event(0,0,0,0)
        
        ev_sample_len = e - s
        
        if sample_rate != nothing
            push!(events["start"], s / sample_rate)
            push!(events["length"], ev_sample_len / sample_rate)
        else 
            push!(events["start"], s)
            push!(events["length"], ev_sample_len)
        end
        #events['length'][i] = ev_sample_len

        ev_mean = float(sums[e] - sm) / ev_sample_len
        #events['mean'][i] = ev_mean
        push!(events["mean"], ev_mean)
        variance = max(0.0, (sumsqs[e] - smsq) / ev_sample_len - (ev_mean^2))
        #events['stdv'][i] = np.sqrt(variance)
        push!(events["stdv"], sqrt(variance))
        
        s = e
        sm = sums[e]
        smsq = sumsqs[e]
    
        #events[i] = event
    end

    #if sample_rate != nothing
    #    events["start"] /= sample_rate
    #    events["length"] /= sample_rate
    #end

    return events
end


function run_event_detection(raw_data, sample_rate; window_lengths=[16, 40], thresholds=[8.0, 4.0], peak_height=1.0)

    """Basic, standard even detection using two t-tests

    :param raw_data: ADC values
    :param sample_rate: Sampling rate of data in Hz
    :param window_lengths: Length 2 list of window lengths across
        raw data from which `t_stats` are derived
    :param thresholds: Length 2 list of thresholds on t-statistics
    :peak_height: Absolute height a peak in signal must rise below
        previous and following minima to be considered relevant
    """

    sums, sumsqs = compute_sum_sumsq(raw_data)

    tstats = []
    for (i, w_len) in enumerate(window_lengths)
        tstat = compute_tstat(sums, sumsqs, w_len, pooled_var=false)
        push!(tstats, tstat)
    
        #length = len(sums)
        #tstat = np.zeros_like(sums)
        #f(sums, sumsqs, tstat, length, w_len, pooled_var)
    end

    peaks = short_long_peak_detector(tstats, thresholds, window_lengths, peak_height)

    events = _construct_events(sums, sumsqs, peaks, sample_rate)

    return events
end


end #end module EventDetection
