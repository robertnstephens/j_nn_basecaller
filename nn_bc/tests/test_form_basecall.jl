#! /usr/bin/julia

module TestFormBasecall

using Base.Test

using Config
using GenUtils
using Decoder
using FormBasecall

function run_tests()
    test1()
    test2()
    test3()
end


function test1()
    num_events = 10
    num_features = 12
    kmer = 5
    num_bases = 4

    cols = num_bases ^ kmer
    kmers = gen_kmers()

    desired_seq = "AAAAACATGCTAGA"
    #AAAAA AAAAC AAACA AACAT ACATG CATGC ATGCT TGCTA GCTAG CTAGA
    #1     2     5     20    79    314   232   925   627   457
    #1     2     3     4     5     6     7     8     9     10
    kmer_inds = [1, 2, 5, 20, 79, 314, 232, 925, 627, 457]

    rand_seed = 1234
    srand(rand_seed)
    post  = rand(num_events, cols)
    post /= 10
    #post  = zeros(num_events, cols)
    
    srand(rand_seed)
    #trans = rand(num_events, num_features)
    trans = zeros(num_events, num_features)

    for (i, kmer_ind) in enumerate(kmer_inds)
        post[i, kmer_ind] += 0.9
    end

    println("########## kmer_inds ###########")
    println(kmer_inds)
    println("################################")

    score, states = run_decoder(post, trans)

    @test length(states) == length(kmer_inds)
    for (i, state) in enumerate(states)
        @test state == kmer_inds[i]
    end

    qdata = get_qdata(post, kmers) #num events * _KMER_LEN
    seq, qual, kmer_path = form_basecall(qdata, kmers, states)

    @test length(seq) == length(desired_seq)
    @test seq == desired_seq

    println(seq)

end


function test2()
    num_events = 9
    num_features = 12
    kmer = 5
    num_bases = 4

    cols = num_bases ^ kmer
    kmers = gen_kmers()

    desired_seq = "AAAAACATGCTAGA"  #skip 2
    #AAAAA AAAAC AAACA AACAT ACATG ATGCT TGCTA GCTAG CTAGA
    #1     2     5     20    79    232   925   627   457
    #1     2     3     4     5     6     7     8     9  
    kmer_inds = [1, 2, 5, 20, 79, 232, 925, 627, 457]

    rand_seed = 1234
    srand(rand_seed)
    post  = rand(num_events, cols)
    post /= 10
    #post  = zeros(num_events, cols)
    
    srand(rand_seed)
    #trans = rand(num_events, num_features)
    trans = zeros(num_events, num_features)

    for (i, kmer_ind) in enumerate(kmer_inds)
        post[i, kmer_ind] += 0.9
    end

    println("########## kmer_inds ###########")
    println(kmer_inds)
    println("################################")

    score, states = run_decoder(post, trans)

    @test length(states) == length(kmer_inds)
    for (i, state) in enumerate(states)
        @test state == kmer_inds[i]
    end


    qdata = get_qdata(post, kmers) #num events * _KMER_LEN
    seq, qual, kmer_path = form_basecall(qdata, kmers, states)

    @test length(seq) == length(desired_seq)
    @test seq == desired_seq

    println(seq)

end


function test3()
    num_events = 8
    num_features = 12
    kmer = 5
    num_bases = 4

    cols = num_bases ^ kmer
    kmers = gen_kmers()

    desired_seq = "AAAAACATGCTAGA" #skip 3
    #AAAAA AAAAC AAACA AACAT ACATG TGCTA GCTAG CTAGA
    #1     2     5     20    79    925   627   457
    #1     2     3     4     5     6     7     8     
    kmer_inds = [1, 2, 5, 20, 79, 925, 627, 457]

    rand_seed = 1234
    srand(rand_seed)
    post  = rand(num_events, cols)
    post /= 10
    #post  = zeros(num_events, cols)
    
    srand(rand_seed)
    #trans = rand(num_events, num_features)
    trans = zeros(num_events, num_features)

    for (i, kmer_ind) in enumerate(kmer_inds)
        post[i, kmer_ind] += 0.9
    end

    println("########## kmer_inds ###########")
    println(kmer_inds)
    println("################################")

    score, states = run_decoder(post, trans)

    @test length(states) == length(kmer_inds)
    for (i, state) in enumerate(states)
        @test state == kmer_inds[i]
    end

    qdata = get_qdata(post, kmers) #num events * _KMER_LEN
    seq, qual, kmer_path = form_basecall(qdata, kmers, states)

    @test length(seq) == length(desired_seq)
    @test seq == desired_seq

    println(seq)

end


end

