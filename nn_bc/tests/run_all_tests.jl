#! /usr/bin/julia

############################################
# author: Robert Stephens                  #
# email:  robert.stephens@nanoporetech.com #
############################################

include("../utils/Config.jl")
include("../utils/GenUtils.jl")
include("../ReadFast5File.jl")
include("../Segment.jl")
include("../Features.jl")
include("../neuralnet/Decoder.jl")
include("../basecall/FormBasecall.jl")
include("../neuralnet/Network.jl")


include("./test_config.jl")
using TestConfig

include("./test_form_basecall.jl")
using TestFormBasecall

include("./test_gen_features.jl")
using TestGenFeatures

include("./test_read_fast5_file.jl")
using TestReadFast5File

include("./test_decoder.jl")
using TestDecoder

include("./test_run_network.jl")
using TestRunNetwork


TestConfig.run_tests()

TestFormBasecall.run_tests()

TestGenFeatures.run_tests()

TestReadFast5File.run_tests()

TestDecoder.run_tests()

TestRunNetwork.run_tests()



