#! /usr/bin/julia

type MyType
    a::Int64
    b::Float64
end

x = MyType(3, 4)

x.a
#EDIT: Methods are defined outside the type definition, e.g.

function double(x::MyType)
    return x.a *= 2
end

#Methods do not live inside the type, as they would do in C++ or Python, for example. 
#This allows one of the key feature of Julia, multiple dispatch, to work also with user-defined types, 
#which are on exactly the same level as system-defined types.

println(x.a)
println(x.b)


res = double(x)
println(string(x.a) * ", " * string(x.b)) 


