############################################
# author: Robert Stephens                  #
# email:  robert.stephens@nanoporetech.com #
############################################

module TestReadFast5File

using Base.Test

using ReadFast5File


function run_tests()
    events, strand_id = read_events("./tests/test.fast5")

    @test_approx_eq(events["mean"][1], 83.82915651674296)
    @test events["length"][1] == 7
    @test_approx_eq(events["mean"][100], 97.12835478605793)
    @test events["length"][100] == 13

end

end

