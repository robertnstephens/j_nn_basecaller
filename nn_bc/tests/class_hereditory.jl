#! /usr/bin/julia


abstract Inheritable
 
+(a::Inheritable, b::Inheritable) = (function (a::Inheritable, b::Inheritable)
    properties = Dict{String, Any}()
 
    for property in names(a)
        propertyName = string(property)
 
        if (!haskey(properties, propertyName))
            properties[propertyName] = (propertyName, string(fieldtype(a, property)))
        else
            (fieldName, fieldType) = properties[propertyName]
            properties[propertyName] = (fieldName, "Any")
        end
    end
 
    for property in names(b)
        propertyName = string(property)
 
        if (!haskey(properties, propertyName))
            properties[propertyName] = (propertyName, string(fieldtype(b, property)))
        else
            (fieldName, fieldType) = properties[propertyName]
            properties[propertyName] = (fieldName, "Any")
        end
    end
 
    fieldCode = ""
 
    for property in values(properties)
        (fieldName, fieldType) = property
        fieldCode = fieldCode * fieldName * "::" * fieldType * "\n"
    end
 
    randomTypeName = "An" * randstring(16);
 
    typeCode = "type " * randomTypeName * " <: Inheritable " * fieldCode * " function " * randomTypeName * "() return new () end end"
 
    eval(parse(typeCode))
 
    randomTypeName = symbol(randomTypeName)
 
    c = @eval begin
        $randomTypeName()
    end
 
    for property in names(a)
        try
            #c.(property) = a.(property)
            #WARNING: deprecated syntax "c.(property) = ...".
            #Use "setfield!(c, property, ...)" instead.


            setfield!(c, property, a.(property) )
        catch
            c
        end
    end
 
    for property in names(b)
        try
            #c.(property) = b.(property)
        
            setfield!(c, property, b.(property) )
        catch
            c
        end
    end
 
    return c
end)(a, b)



type A <: Inheritable
    whoAmI::Function
    uniqueFunctionA::Function
 
    function A()
        instance = new()
 
        instance.whoAmI = function ()
            println("I am object A")
        end
 
        instance.uniqueFunctionA = function ()
            println("Function unique to A")
        end
 
        return instance
    end
end
 
type B <: Inheritable
    whoAmI::Function
    uniqueFunctionB::Function
 
    function B()
        instance = new()
 
        instance.whoAmI = function ()
            println("I am object B")
        end
 
        instance.uniqueFunctionB = function ()
            println("Function unique to B")
        end
 
        return A() + instance
    end
end

#Type A is a standard type declaration, using the same emulated method bundling from part 1. 
#Type B extends type A in the constructor, by returning an instance of the merged pseudotype instead of an instance of type B. That code is:

return A() + instance
#The below code is an example of using these two objects:

a = A()
a.whoAmI()
 
b = B()
b.whoAmI()
 
b.uniqueFunctionA()
b.uniqueFunctionB()



