############################################
# author: Robert Stephens                  #
# email:  robert.stephens@nanoporetech.com #
############################################

module TestGenFeatures

using Base.Test


using ReadFast5File
using Features


function run_tests()
    events_out, strand_id = read_events("./tests/test.fast5")
    
    events, features, features_arr = events_to_features(events_out, trim=false)

    println( "num events: " * string(length(events)) ) 
    println( "num features: " * string(length(features["stdv[0]"])) ) 

    #println( features["stdv[-1]"][1] )
    #println( features["mean[-1]"][1] )
    #println( features["length[-1]"][1] )
    #println("second -1")
    #println( features["mean[-1]"][2] )
    #println( features["stdv[0]"][1] )
    #println( features["mean[0]"][1] )
    #println( features["length[0]"][1] )
    #println( features["stdv[1]"][1] )
    #println( features["mean[1]"][1] )
    #println( features["length[1]"][1] )

    #pre scaling around mean
    #@test_approx_eq(events[1].mean, features["mean[-1]"][2])
    #@test_approx_eq(events[1].mean, features["mean[0]"][1])
    #@test_approx_eq(events[2].mean, features["mean[0]"][2])
    #@test_approx_eq(events[2].mean, features["mean[1]"][1])
    #@test_approx_eq(events[3].mean, features["mean[1]"][2])


    @test_approx_eq(0.0, features["mean[-1]"][1])
    #TODO 
    #@test_approx_eq(features["mean[-1]"][2], 0.012571145159517596)

    len = length(features["mean[1]"]) 
    @test_approx_eq(0.0, features["mean[1]"][len])
    
end

#run_tests()
end

