############################################
# author: Robert Stephens                  #
# email:  robert.stephens@nanoporetech.com #
############################################

module TestConfig

using Base.Test

#include("../utils/Config.jl")
using Config

export run_tests
function run_tests()

    kmers = gen_kmers()

    @test kmers[1]     == "AAAAA"
    @test kmers[2]     == "AAAAC"
    @test kmers[end-2] == "TTTTG"
    @test kmers[end-1] == "TTTTT"
    @test kmers[end]   == "XXXXX"


    #for (i, k) in enumerate(kmers)
    #    println(string(i) * " : " * join(k))
    #end

end

end

