#! /usr/bin/julia

module ReadFast5Events

export read_events

using HDF5

for p in ("Knet","AutoGrad","ArgParse","Compat")
    Pkg.installed(p) == nothing && Pkg.add(p)
end


immutable Event
    mean::Float64
    stdv::Float64
    start::Int64
    length::Int64
end


function print_summary(a)
    println(summary(a), ": ", repr(a) )
end

function read_events(filename)

    println("reading: ", filename)

    fid=h5open(filename,"r")

    #TODO: find automatically
    #event_dataset_name = "Analyses/EventDetection_000/Reads/Read_104/Events"
   
    #read_groups = "Analyses/EventDetection_000/Reads/Read_104/"
    read_groups = "Analyses/EventDetection_000/Reads/"
    
    g=fid[read_groups]

 
    #g = fid["mygroup"]
    #dset = g["mydataset"]

    for group_name in names(g)

        println("group_name: " * group_name) #/Read_104
        
        g2=fid[read_groups * "/" * group_name]
        
        if exists(g2, "Events")
            println("has dataset Events")

            event_dataset_name = read_groups * "/" * group_name * "/Events"

            dset=fid[event_dataset_name]
            event_data=read(dset)
            
            @eval function Base.read(io::IO, ::Type{Event})
                Event($([:(read(io, $(x))) for x in Event.types]...))
            end
            
            buf = IOBuffer(event_data.data)
            events = Event[]
            while !eof(buf)
                push!(events, read(buf, Event))
            end
        
            means = Float64[]
            lengths = Int64[]    
        
            for event in events
                #println(event.mean)
                push!(means, event.mean) 
                push!(lengths, event.length)
            end
        
            println("closing file") 
            close(fid)
        
            return means, lengths


        else 
            println("no dataset Events")
        end

    end


end


function main()

    for filename in ARGS
        means, lengths = read_events(filename)
        
        #println("first event: " * filename * ", " * string(mean[1]) * ", " * string(lengths[1]) ) 
        println("first event: " * filename * ", " * string(means[1]) * ", " * string(lengths[1]) ) 
    
    end


end

main()

end

