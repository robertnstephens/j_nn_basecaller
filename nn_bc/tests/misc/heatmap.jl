#! /usr/bin/julia

#TODO only way to fix this error is using generic binaries not PPA, so need to upgrade Linux distro version?

#WARNING: Error during initialization of module GMP:
#ErrorException("The dynamically loaded GMP library (version 5.1.3 with __gmp_bits_per_limb == 64)
#does not correspond to the compile time version (version 6.1.0 with __gmp_bits_per_limb == 64).
#Please rebuild Julia."


using PyPlot

M = rand(10, 10)
pcolormesh(M)
colorbar()
