############################################
# author: Robert Stephens                  #
# email:  robert.stephens@nanoporetech.com #
############################################

module TestDecoder

using Base.Test

#include("../utils/Config.jl")
using Config

#include("../neuralnet/Decoder.jl")
using Decoder


function run_tests()
    test1()
    test2()
    test3()
    test4()

    #AAAAACATGCTAGA
    #AAAAA AAAAC AAACA AACAT ACATG CATGC ATGCT TGCTA GCTAG CTAGA
    #1     2     5     20    79    314   232   925   627   457
    #1     2     3     4     5     6     7     8     9     10
    #kmer_inds = [1, 2, 5, 20, 79, 314, 232, 925, 627, 457]


    #AAAAACATGCTAGA          #skip 2           #stay
    #AAAAA AAAAC AAACA AACAT ACATG ATGCT TGCTA TGCTA GCTAG CTAGA
    #1     2     5     20    79    232   925   925   627   457
    #1     2     3     4     5     6     7     8     9     10
    kmer_inds = [1, 2, 5, 20, 79, 232, 925, 925, 627, 457]

    test_estimate_transitions_py(kmer_inds)
    test_estimate_transitions_cpp(kmer_inds)
end

function test1()

    num_events = 100
    num_features = 12
    kmer = 5
    num_bases = 4

    cols = num_bases ^ kmer

    rand_seed = 1234

    srand(rand_seed)
    post  = rand(num_events, cols)
    srand(rand_seed)
    trans = rand(num_events, num_features)

    score, states = run_decoder(post, trans)

    println("score")
    println(score)
    println("states")
    println(states)

    @test_approx_eq(score, -130.4580973550018)
    
    desired_states = [150,150,150,150,150,150,150,150,598,598,598,598,342,342,342,342,341,340,340,333,333,333,333,333,333,333,306,798,798,119,119,119,119,119,473,866,866,866,866,866,866,390,390,390,535,535,91,425,425,425,425,674,674,674,544,544,544,544,544,544,127,127,127,127,127,127,506,997,997,997,997,913,913,913,577,577,258,258,258,8,8,8,32,32,32,32,128,128,128,510,510,510,1016,990,990,990,888,888,888,888]

    @test length(states) == length(desired_states)

    for i in 1:length(states)
        @test states[i] == desired_states[i]
    end

end


function test2()
    num_events = 10
    num_features = 12
    kmer = 5
    num_bases = 4

    cols = num_bases ^ kmer

    #AAAAACATGCTAGA
    #AAAAA AAAAC AAACA AACAT ACATG CATGC ATGCT TGCTA GCTAG CTAGA
    #1     2     5     20    79    314   232   925   627   457
    #1     2     3     4     5     6     7     8     9     10
    kmer_inds = [1, 2, 5, 20, 79, 314, 232, 925, 627, 457]

    rand_seed = 1234
    srand(rand_seed)
    post  = rand(num_events, cols)
    post /= 10
    #post  = zeros(num_events, cols)
    
    srand(rand_seed)
    #trans = rand(num_events, num_features)
    trans = zeros(num_events, num_features)

    for (i, kmer_ind) in enumerate(kmer_inds)
        post[i, kmer_ind] += 0.9
    end

    println("########## kmer_inds ###########")
    println(kmer_inds)
    println("################################")

    score, states = run_decoder(post, trans)

    @test length(states) == length(kmer_inds)
    for (i, state) in enumerate(states)
        @test state == kmer_inds[i]
    end

    println("score")
    println(score)
    println("states")
    println(states)

end


function test3()
    num_events = 9
    num_features = 12
    kmer = 5
    num_bases = 4

    cols = num_bases ^ kmer

    #AAAAACATGCTAGA          #skip 2
    #AAAAA AAAAC AAACA AACAT ACATG ATGCT TGCTA GCTAG CTAGA
    #1     2     5     20    79    232   925   627   457
    #1     2     3     4     5     6     7     8     9  
    kmer_inds = [1, 2, 5, 20, 79, 232, 925, 627, 457]

    rand_seed = 1234
    srand(rand_seed)
    post  = rand(num_events, cols)
    post /= 10
    #post  = zeros(num_events, cols)
    
    srand(rand_seed)
    #trans = rand(num_events, num_features)
    trans = zeros(num_events, num_features)

    for (i, kmer_ind) in enumerate(kmer_inds)
        post[i, kmer_ind] += 0.9
    end

    println("########## kmer_inds ###########")
    println(kmer_inds)
    println("################################")

    score, states = run_decoder(post, trans)

    @test length(states) == length(kmer_inds)
    for (i, state) in enumerate(states)
        @test state == kmer_inds[i]
    end

    println("score")
    println(score)
    println("states")
    println(states)

end


function test4()
    num_events = 8
    num_features = 12
    kmer = 5
    num_bases = 4

    cols = num_bases ^ kmer

    #AAAAACATGCTAGA          #skip 3
    #AAAAA AAAAC AAACA AACAT ACATG TGCTA GCTAG CTAGA
    #1     2     5     20    79    925   627   457
    #1     2     3     4     5     6     7     8     
    kmer_inds = [1, 2, 5, 20, 79, 925, 627, 457]

    rand_seed = 1234
    srand(rand_seed)
    post  = rand(num_events, cols)
    post /= 10
    #post  = zeros(num_events, cols)
    
    srand(rand_seed)
    #trans = rand(num_events, num_features)
    trans = zeros(num_events, num_features)

    for (i, kmer_ind) in enumerate(kmer_inds)
        post[i, kmer_ind] += 0.9
    end

    println("########## kmer_inds ###########")
    println(kmer_inds)
    println("################################")

    score, states = run_decoder(post, trans)

    @test length(states) == length(kmer_inds)
    for (i, state) in enumerate(states)
        @test state == kmer_inds[i]
    end

    println("score")
    println(score)
    println("states")
    println(states)

end



function test_estimate_transitions_py(kmer_inds)

    num_events = length(kmer_inds)
   
    post = zeros(num_events, _NUM_KMERS)
    for ev in 1:num_events
        post[ev, kmer_inds[ev]] += 0.75
    end

    res = zeros( (size(post, 1), 3) )
    fill!(res, _ETA)

    trans = estimate_transitions_py(post, res)

    println("######### test_estimate_transitions_py #########")

    for i in 1:size(trans, 1)
        println( trans[i,:] )
    end

    println("######## end test_estimate_transitions_py ########")

end



function test_estimate_transitions_cpp(kmer_inds)

    num_events = length(kmer_inds)
   
    post = zeros(num_events, _NUM_KMERS)
    for ev in 1:num_events
        post[ev, kmer_inds[ev]] += 0.75
    end

    res = zeros( (size(post, 1), 3) )
    fill!(res, _ETA)

    trans = estimate_transitions_cpp(post, res)

    println("######### test_estimate_transitions_cpp #########")

    for i in 1:size(trans, 1)
        println( trans[i,:] )
    end

    println("######## end test_estimate_transitions_cpp ########")

end


end

