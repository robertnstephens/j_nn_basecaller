############################################
# author: Robert Stephens                  #
# email:  robert.stephens@nanoporetech.com #
############################################

module ReadFast5File

export read_events, read_raw_data

using HDF5

for p in ("Knet","AutoGrad","ArgParse","Compat")
    Pkg.installed(p) == nothing && Pkg.add(p)
end


immutable Event
    mean::Float64
    stdv::Float64
    start::Int64
    length::Int64
end


function print_summary(a)
    println(summary(a), ": ", repr(a) )
end

function read_events(filename)
    
    println("reading events: ", filename)

    strand_id = read_strand_id(filename)

    #HDF5 "./input_data/904896_ch170_read104_strand.fast5" 
    # "904896_ch170_read104_strand"

    fid=h5open(filename,"r")

    #TODO: ability to read multiple reads in one file
    read_groups = "Analyses/EventDetection_000/Reads/"

    g=fid[read_groups]

    for group_name in names(g)

        println("group_name: " * group_name) #/Read_104
        g2=fid[read_groups * "/" * group_name]

        if exists(g2, "Events")
            println("has dataset Events")

            event_dataset_name = read_groups * "/" * group_name * "/Events"
    
            dset=fid[event_dataset_name]
            event_data=read(dset)
            
            @eval function Base.read(io::IO, ::Type{Event})
                Event($([:(read(io, $(x))) for x in Event.types]...))
            end
            
            buf = IOBuffer(event_data.data)
            events = Event[]
            while !eof(buf)
                push!(events, read(buf, Event))
            end

            println("closing file") 
            close(fid)

            events_dict = Dict{String, Array{Any,1}}()
            events_dict["mean"], events_dict["stdv"], events_dict["start"], events_dict["length"] = [],[],[],[]
        
            for event in events
                push!(events_dict["mean"], event.mean)
                push!(events_dict["stdv"], event.stdv)
                push!(events_dict["start"], event.start)
                push!(events_dict["length"], event.length)
            end 

            return events_dict, strand_id

        end
    end
end

function read_raw_data(filename; scale=true)
 
    println("reading raw data: ", filename)

    strand_id = read_strand_id(filename) 

    meta_dict = read_meta(filename)
 
    fid=h5open(filename,"r")

    #TODO: ability to read multiple reads in one file
    #read_groups = "Analyses/EventDetection_000/Reads/"
    read_groups = "Raw/Reads/"

    g=fid[read_groups]

    for group_name in names(g)

        println("group_name: " * group_name) #/Read_104
        g2=fid[read_groups * "/" * group_name]

        if exists(g2, "Signal")
            println("has dataset Signal")

            raw_dataset_name = read_groups * "/" * group_name * "/Signal"
    
            dset=fid[raw_dataset_name]
            raw_data=read(dset)
            
            println("closing file") 
            close(fid)

            offset = meta_dict["offset"]
            range = meta_dict["range"] 
            digitisation = meta_dict["digitisation"]
            raw_unit = range / digitisation 
           

            println("offset, range, digitisation, raw_unit")
            println(string(offset) * ", " * string(range) * ", " * string(digitisation) * ", " * string(raw_unit))

            # Scale data to pA
            if scale
                raw_data = (raw_data + offset) * raw_unit            
            end

            return raw_data, meta_dict, strand_id
        end 
    end
end

function read_strand_id(filename)

    #HDF5 "./input_data/904896_ch170_read104_strand.fast5" 
    # "904896_ch170_read104_strand"

    strand_id = split(split(filename, "/")[end], ".fast5")[1]
    println("strand_id:") 
    println(strand_id)
    
    return strand_id

end


function read_meta(filename)
    
    meta_group = "UniqueGlobalKey/channel_id/"
    meta_fields = ["channel_number", "digitisation", "offset", "range", "sampling_rate"]
    meta_dict = Dict{String,Float64}()

    fid = h5open(filename,"r")

    g=fid[meta_group]

    for field in meta_fields

        tf = exists(attrs(g), field)
        if tf == false
            println("ERROR: no attribute " * field)
        else
            meta_dict[field] = read(attrs(g), field )
            #println(field * " : ", read( attrs(g), field ))
        end
    end

    close(fid)

    return meta_dict
end

end #end module ReadFast5File

